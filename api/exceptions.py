#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.exceptions import APIException

class OutOfQuota(APIException):

    status_code = 413
    default_detail = 'Quota limit is reached'
    default_code = 'service_unavailable'