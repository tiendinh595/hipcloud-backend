from rest_framework import authentication
from rest_framework.exceptions import AuthenticationFailed
from cas.models.access_token import AccessToken
from django.conf import settings
from jose import jwt

class TokenAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token)
                except Exception as ex:
                    raise AuthenticationFailed('Token not found')
                return (token, token.user_id)
            else:
                raise AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)


class TokenAuthenticationGet(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.GET.get('auth')
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token)
                except:
                    raise AuthenticationFailed('Token not found')
                return (token, token.user)
            else:
                raise AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)


class BackendTokenAuthenticate(authentication.BaseAuthentication):
    #
    #  {
    #     "iss": "user_id",
    #     "iat": 1300819370,
    #     "exp": 1300819380,
    #     "mth": "fb/gg/em/mo",
    #     "tok": "token",
    #     "sub": "subject_id",
    #     "context": {
    #         "profile": {
    #             "id": "batman",
    #             "username": "bat_wayne"
    #       }
    #      },
    #     "admin": False
    # }

    def authenticate(self, request):
        jwt_token = request.META.get('HTTP_AUTHORIZATION')
        try:
            if jwt_token:
                try:
                    json_profile = jwt.decode(jwt_token, settings.JWT_PRIVATE_SIGNATURE, algorithms=['HS256'])
                except Exception as ex:
                    raise AuthenticationFailed('Token not found')
                return (json_profile, json_profile['iss'])
            else:
                raise AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)