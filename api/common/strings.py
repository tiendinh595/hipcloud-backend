from django.conf import settings
from datetime import datetime , timedelta
import  uuid
import random
import string
import pytz

utc = pytz.UTC

def random_string_number(char_len=32):
    return ''.join([random.choice(string.digits) for n in xrange(char_len)])

def random_string(char_len=32):
    return ''.join([random.choice(string.ascii_letters + string.digits) for n in xrange(char_len)])

def pkgen():
    return str(uuid.uuid4().hex)

def uugen():
    return ''.join([random.choice(string.ascii_letters + string.digits).upper() for n in xrange(12)])

def get_current_domain(app_name, api):
    return settings.DOMAIN + "/backend/" + app_name + "/" + api

# 09:28:16 Jul 11, 2016 PDT
# Wed Jul 06 2016 10:13:56 GMT 0700 (ICT)
def convert_to_datetime(date_string):
    formatting = ""
    if "PDT" in date_string:
        formatting = '%H:%M:%S %b %d, %Y PDT'
    else:
        return None

    try:
        return (datetime.strptime(date_string, formatting) + timedelta(hours=7)).strftime("%Y-%m-%d %H:%M:%S")
    except Exception as ex:
        print str(ex)
        return None