FROM python:2.7-slim
MAINTAINER Nguyen Trung <medhap369@gmail.com>

RUN apt-get update && apt-get install -y supervisor && apt-get install -y libmysqlclient-dev && apt-get install -y build-essential && apt-get install -y libssl-dev && apt-get install -y libffi-dev && pip install uwsgi && useradd -m backend

ADD . /home/backend

RUN  mkdir -p /home/backend/logs && touch /home/backend/logs/app.log && chmod 777 /home/backend/logs/app.log  && touch /home/backend/logs/income_request.log && chmod 777 /home/backend/logs/income_request.log  && cd /home/backend && pip install -r requirements.txt && cp /home/backend/supervisor.conf /etc/supervisor/conf.d && python manage.py collectstatic --noinput --settings=hipcloud_backend.settings.prod

WORKDIR /home/backend

CMD ["/usr/bin/supervisord"]