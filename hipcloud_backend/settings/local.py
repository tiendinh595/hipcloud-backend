from base import *

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DEBUG = True
ALLOWED_HOSTS = ['*']
BLOCK_URL = 'http://localhost:8000'
RESET_PASSWORD_URL ='http://localhost:8000/pages/new_password'
SYNC_CONTAINER_URL = 'http://localhost:8000/sync/container'

FACEBOOK_APP_ID = '1216928088359972'
FACEBOOK_APP_SECRET = '2850e51691dfb6ec5b5f655705b94ea5'

RECAPTCHA_URL = 'https://www.google.com/recaptcha/api/siteverify'
RECAPTCHA_PUBLIC_KEY = '6LdGbw8UAAAAAETD65n0MmkoNm8KmkauNwA1OWYL'
RECAPTCHA_SECRET_KEY = '6LdGbw8UAAAAAFkhID_Yc9QBpQGIWPOt2Su_eWeF'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'backend_storage',
        'USER': 'root',
        'PASSWORD': 'root',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'formatters': {
        'logstash': {
            '()': 'logstash_formatter.LogstashFormatterV1',
            'format': '{"extra":{"app": "hipcloud_block_storage"}}'
        },
        'verbose': {
            'format' : "%(levelname)s : [%(asctime)s] [%(name)s:%(funcName)s:%(process)d:%(lineno)d] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        'simple': {
            'format': '%(levelname)s : [%(asctime)s] %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.handlers.RotatingFileHandler',
            'filename': SITE_ROOT + '/logs/app.log',
            'maxBytes': 1024 * 1024 * 10,  # 10 MB
            'backupCount': 10,
            'formatter':'logstash',
        },
        'request_handler': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': SITE_ROOT + '/logs/income_request.log',
            'maxBytes': 1024*1024*5, # 5 MB
            'backupCount': 5,
            'formatter':'logstash',
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
        },
    },
    'loggers': {
        '': {
            'handlers': ['file','console'],
            'level': 'INFO',
            'propagate': True
        },
        'django': {
            'handlers':['file','console'],
            'propagate': True,
            'level':'INFO',
        },
        'django.request': {
            'handlers': ['request_handler','console'],
            'level': 'INFO',
            'propagate': False
        }
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://127.0.0.1:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 200},
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
        }
    }
}

EMAIL_BACKEND = "sgbackend.SendGridBackend"
SENDGRID_API_KEY = "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"


# SESSION_ENGINE = "django.contrib.sessions.backends.cache"
# SESSION_CACHE_ALIAS = "default"

STATIC_URL = '/cas/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')