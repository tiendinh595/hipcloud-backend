from base import *

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DEBUG = True

ALLOWED_HOSTS = ['*']
BLOCK_URL = 'https://api-dev.upbox.vn/block'
CONFIRM_EMAIL_URL = 'https://api-dev.upbox.vn/cas/register'
RESET_PASSWORD_URL ='https://api-dev.upbox.vn/cas/pages/new_password'
SYNC_CONTAINER_URL = 'https://api-dev.upbox.vn/block/sync/container'

FACEBOOK_APP_ID = '1216928088359972'
FACEBOOK_APP_SECRET = '2850e51691dfb6ec5b5f655705b94ea5'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'backend_storage',
        'USER': 'backend_storage',
        'PASSWORD': 'backend@!!!',
        'HOST': 'mysql-dev',
        'PORT': '3306',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'standard': {
            'format': '%(asctime)s [%(levelname)s] %(name)s: %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': 'logs/app.log',
            'maxBytes': 1024*1024*20, # 10 MB
            'backupCount': 10,
            'formatter':'verbose',
        },
        'request_handler': {
            'level':'DEBUG',
            'class':'logging.handlers.RotatingFileHandler',
            'filename': 'logs/income_request.log',
            'maxBytes': 1024*1024*20, # 5 MB
            'backupCount': 5,
            'formatter':'standard',
        }
    },
    'loggers': {
        '': {
            'handlers': ['file'],
            'level': 'INFO',
            'propagate': True
        },
        'django.request': {
            'handlers': ['request_handler'],
            'level': 'INFO',
            'propagate': False
        }
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis-dev:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 1000},
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
        }
    }
}


# SESSION_ENGINE = "django.contrib.sessions.backends.cache"
# SESSION_CACHE_ALIAS = "default"

EMAIL_BACKEND = "sgbackend.SendGridBackend"
SENDGRID_API_KEY = "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"

STATIC_URL = '/cas/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

CELERY_BROKER_URL = 'amqp://upbox_user:65q10LCouJg525Q8f0yY@rabbitmq-dev:5672//upbox'

CELERY_RESULT_BACKEND = 'redis://redis-dev:6379/8'