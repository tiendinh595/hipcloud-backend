import os
from os.path import abspath, basename, dirname


# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))

DJANGO_ROOT = dirname(dirname(abspath(__file__)))

# Absolute filesystem path to the top-level project folder.
SITE_ROOT = dirname(DJANGO_ROOT)

# Site name.
SITE_NAME = basename(DJANGO_ROOT)

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.9/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'f2+o)+28b8!%a(ak(zw6rjfy%6lww@m$bup7vk4bax!k%o$e0d'

JWT_PRIVATE_SIGNATURE = "Y2auJS_2X,Btg6o7J+CVS5c7lHOr;a"

# Application definition

INSTALLED_APPS = [
    # 'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'rest_framework_swagger',
    'rest_framework',
    'cas',
    'billing',
    'api'
]

MIDDLEWARE_CLASSES = [
    # 'django.middleware.cache.UpdateCacheMiddleware',
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    # 'django.middleware.cache.FetchFromCacheMiddleware'
]

ROOT_URLCONF = 'hipcloud_backend.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'templates')]
        ,
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

PASSWORD_HASHERS = [
    'django.contrib.auth.hashers.BCryptSHA256PasswordHasher',
    'django.contrib.auth.hashers.BCryptPasswordHasher',
    'django.contrib.auth.hashers.PBKDF2PasswordHasher',
    'django.contrib.auth.hashers.PBKDF2SHA1PasswordHasher',
    'django.contrib.auth.hashers.SHA1PasswordHasher',
    'django.contrib.auth.hashers.MD5PasswordHasher',
    'django.contrib.auth.hashers.CryptPasswordHasher',
]

REST_FRAMEWORK = {
    'EXCEPTION_HANDLER': 'api.utils.custom_exception_handler'
}

ANDROID_PACKAGE_ID = "vn.com.upbox.app.and"
ANDROID_SUBSCRIPTION_ID = "vn.com.upbox.app.and.subscription1"

APPLE_BUNDLE_ID = "com.upbox.app.ios"

WSGI_APPLICATION = 'hipcloud_backend.wsgi.application'

LANGUAGE_CODE = 'en-us'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True

ANDROID_DEVELOPER_KEY =  DJANGO_ROOT + "/keys/Upbox_Google_Play_Android_Developer-b60c52c6bdb2.json"
ANDROID_SCOPES = ['https://www.googleapis.com/auth/androidpublisher']

from oauth2client.service_account import ServiceAccountCredentials
from apiclient import discovery

# credentials = ServiceAccountCredentials.from_json_keyfile_name(ANDROID_DEVELOPER_KEY, ANDROID_SCOPES)
# ANDROID_SERVICE = discovery.build('androidpublisher', 'v2', credentials=credentials)

FIREBASE_PUSH_API = "https://fcm.googleapis.com/fcm/send"
FIREBASE_KEY = "key=AAAATmJx1UQ:APA91bFfimM9eWjZkStnD86E-Dl3vIEML6RXBcRvJUds3fvXn15yk3eU5xHMoAbozv7fhoI3qS8mq2axGYZkyM9S96yWXNSECcneWIFzQDsxfWffXgohs8ANQlsfQgE2Jj_dCn31lsnu5u-0Xf64NAEWzbaJJen5zQ"

SHARED_APPLE_KEY = "9fc75f4c50744f4dbe459c3649d939ec"
# TEST_RUNNER = 'django_coverage.coverage_runner.CoverageRunner'


from kombu import Queue, Exchange
CELERY_TASK_QUEUES = (
    Queue('default', Exchange('default_queue', 'direct'), queue_arguments={'x-max-priority': 10}),
    Queue('backend.subscription', Exchange('backend.subscription', delivery_mode=Exchange.PERSISTENT_DELIVERY_MODE),
          durable=True, queue_arguments={'x-max-priority': 10}),
)

CELERY_TASK_ROUTES = {
    'billing.tasks.subscription.check_subscription': {
        'queue': 'backend.subscription',
    },
}

from celery.schedules import crontab
CELERY_BEAT_SCHEDULE = {
    'ehour-check-subscription': {
        'task': 'billing.tasks.subscription.check_subscription',
        'schedule': crontab(minute='30', hour='*'),
        'args': (None, ),
        'options': {
            'expires': 60*10,
            'queue': 'backend.subscription',
        }
    },
}

DEFAULT_QUOTA_CONTAINER = 21474836480

CELERY_TIMEZONE = 'UTC'

CELERY_QUEUE_HA_POLICY = 'all'

CELERY_DEFAULT_QUEUE = 'default'

CELERY_TASK_QUEUE_MAX_PRIORITY = 10

CELERY_REMOTE_TRACEBACKS = True

CELERY_TRACK_STARTED = True

CELERY_ACKS_LATE = True

CELERY_SEND_TASK_SENT_EVENT = True

CELERY_TASK_SERIALIZER = 'json'

CELERY_TASK_PUBLISH_RETRY = True

CELERY_IGNORE_RESULT = False

CELERY_STORE_ERRORS_EVEN_IF_IGNORED = True

CELERYD_TASK_TIME_LIMIT = 60 * 60

CELERY_TASK_CREATE_MISSING_QUEUES = True

CELERY_DEFAULT_DELIVERY_MODE = "persistent"

CELERY_RESULT_SERIALIZER = 'json'

CELERY_RESULT_PERSISTENT = True

RESULT_EXPIRES = 60 * 10

CELERY_ACCEPT_CONTENT = ['json']

CELERYD_PREFETCH_MULTIPLIER = 1

CELERYD_MAX_TASKS_PER_CHILD = 1000

CELERYD_POOL_RESTARTS = True

CELERY_WORKER_DIRECT = False

CELERY_ENABLE_REMOTE_CONTROL = True

CELERYD_SEND_EVENTS = True

CELERY_REDIS_MAX_CONNECTIONS = 4

BROKER_HEARTBEAT = 60

BROKER_HEARTBEAT_CHECKRATE = 2

BROKER_POOL_LIMIT = 8
