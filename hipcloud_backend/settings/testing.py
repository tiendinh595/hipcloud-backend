from base import *

# Database
# https://docs.djangoproject.com/en/1.9/ref/settings/#databases

DEBUG = True

ALLOWED_HOSTS = ['*']
BLOCK_URL = 'http://api.upbox.vn/block'
CONFIRM_EMAIL_URL = 'http://api.upbox.vn/cas/register'
RESET_PASSWORD_URL ='http://api.upbox.vn/cas/pages/new_password'
SYNC_CONTAINER_URL = 'http://api.upbox.vn/block/sync/container'


FACEBOOK_APP_ID = '1216928088359972'
FACEBOOK_APP_SECRET = '2850e51691dfb6ec5b5f655705b94ea5'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'test_backend_storage',
        'USER': 'root',
        'PASSWORD': 'storage@2016',
        'HOST': 'mysql',
        'PORT': '3306',
    }
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format' : "[%(asctime)s] %(levelname)s [%(name)s:%(lineno)s] %(message)s",
            'datefmt' : "%d/%b/%Y %H:%M:%S"
        },
        'simple': {
            'format': '%(levelname)s %(message)s'
        },
    },
    'handlers': {
        'file': {
            'level': 'DEBUG',
            'class': 'logging.FileHandler',
            'filename': './log/backend.log',
            'formatter': 'verbose'
        },
    },
    'loggers': {
        'django': {
            'handlers':['file'],
            'propagate': True,
            'level':'DEBUG',
        }
    }
}

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 1000},
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
        }
    }
}

EMAIL_BACKEND = "sgbackend.SendGridBackend"
SENDGRID_API_KEY = "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"


# SESSION_ENGINE = "django.contrib.sessions.backends.cache"
# SESSION_CACHE_ALIAS = "default"

STATIC_URL = '/cas/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')