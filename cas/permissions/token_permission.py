from rest_framework import permissions


class TokenIsAuthenticated(permissions.IsAuthenticated):

    def has_permission(self, request, view):
        if request.user:
            return request.user