#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from cas.models.feedback import FeedBack

class FeedbackSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('user', 'content', 'created_at')
        model = FeedBack