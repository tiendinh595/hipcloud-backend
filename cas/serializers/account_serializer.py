from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers
from cas.models.account import Account

class AccountSerializer(serializers.ModelSerializer):

    id = serializers.CharField(read_only=True)

    email = serializers.ReadOnlyField(source='email.email')
    mobile = serializers.ReadOnlyField(source='mobile.phone')
    created_at = serializers.SerializerMethodField()

    uu_code = serializers.CharField(read_only=True)

    class Meta:
        fields = ('id','email','mobile','full_name','dob','gender','address','avatar','created_at',
                  'uu_code')
        model = Account
        depth = 1

    def get_created_at(self, account):
        return account.created_at.strftime("%Y-%m-%dT%H:%M:%SZ")


