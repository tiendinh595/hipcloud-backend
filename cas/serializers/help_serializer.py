#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from rest_framework.validators import UniqueTogetherValidator
from rest_framework import serializers
from cas.models.help_info import HelpInfo

class DefaultHelpInfoSerializer(serializers.ModelSerializer):

    class Meta:
        fields = ('question','detail')
        model = HelpInfo