#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework.views import APIView
from cas.models.help_info import HelpInfo
from cas.serializers.help_serializer import DefaultHelpInfoSerializer
from api.common import response

class HelpInfoView(APIView):

    def post(self, request, *args, **kwargs):

        """
        List Help Question
        ---

        serializer: cas.serializers.help_serializer.DefaultHelpInfoSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: false
              type: string
              paramType: header

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        questions = HelpInfo.objects.filter(status=1)
        response_data = DefaultHelpInfoSerializer(questions, many=True)
        return response.success(response_data.data)