
from rest_framework.views import APIView
from api.common import response
from cas.models import AccessToken
from cas.serializers.account_serializer import AccountSerializer
from jose import jwt
from django.conf import settings
import time, requests

class Logout(APIView):

	def delete(self, request, *args, **kwargs):

  		"""
	        Logout
	        ---
	        type:
	              token:
	                required: true
	                type: string

	        parameters:
	            - name: authorization
	              description: Access token of system.
	              required: true
	              type: string
	              paramType: header

	        responseMessages:
	            - code: 200
	              message: Success
	            - code: 400
	              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
	            - code: 403
	              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
	            - code: 404
	              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
	            - code: 500
	              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

	        consumes:
	            - application/json

	        produces:
	            - application/json
        """

		try:
			token = request.META.get('HTTP_AUTHORIZATION', None)
			access_token = AccessToken.objects.get(id=token)
			local_account = access_token.user

			#SYNC TOKEN
			mth = ''
			if local_account.email is not None:
				if local_account.email.facebook is not None:
			  		mth = 'fb'
			  	elif local_account.email.google is not None:
			  		mth = 'gg'
			  	else:
			  		mth = 'em'
			elif local_account.mobile is not None:
				mth = 'mo'	

			profile_data = AccountSerializer(local_account)

			token_data = {'iss':access_token.user.id, 'iat':int(time.mktime(access_token.created_at.timetuple())),'exp':access_token.expire, 'mth':mth, 'tok':access_token.id, 'sub':access_token.subject_id,  'context':profile_data.data, 'admin':False }
			token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')

			rs = requests.delete(settings.BLOCK_URL+'/private/sync_token/', data={'payload':token})

			if rs.status_code == 200:
				access_token.delete()
			else:
				return response.fail("System errors")
		except Exception as e:
			print e.message
			return response.fail("System errors")

		return response.success({'status':'success'})