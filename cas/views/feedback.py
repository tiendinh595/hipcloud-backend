#!/usr/bin/env python
# -*- coding: utf-8 -*-
from cas.serializers.feedback_serializer import FeedbackSerializer
from rest_framework.exceptions import ParseError
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated
from rest_framework.views import APIView
from cas.models.feedback import FeedBack
from api.common import response



class FeedbackView(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Save user feedback
        ---

        serializer: cas.serializers.help_serializer.DefaultHelpInfoSerializer

        parameters_strategy: replace

        parameters:
            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: false
              type: string
              paramType: header

            - name: content
              description: content feedback
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        content = request.POST.get('content', None)
        if not content:
            raise ParseError("Content not found")

        feedback = FeedBack(user_id = user_id, content = content)
        feedback.save()
        response_data = FeedbackSerializer(feedback)
        return response.success(response_data.data)