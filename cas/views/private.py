import json
import logging

import requests
from django.conf import settings
from django.db.models import Count
from jose import jwt
from rest_framework.views import APIView
from rest_framework.exceptions import ParseError, PermissionDenied

from api.common import response
from cas.models import AccessToken

logger = logging.getLogger(__name__)
class TestView(APIView):
    def get(self, request, *args, **kwargs):
        """
        Test
        ---
        responseMessages:
            - code: 200
              message: '{"message" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = '3defa696c410494a98ad622220a783e8'
        list_token = AccessToken.objects.filter(user_id=user_id).values('push_token').annotate(
            dcount=Count('push_token'))
        for token in list_token:
            print 'token.push_token: %s'%token['push_token']

        return response.success({"status": "success"})


class PrivatePushNotificationView(APIView):
    def verify_token(self, payload):
        try:
            json = jwt.decode(payload, settings.JWT_PRIVATE_SIGNATURE, algorithms=['HS256'])
            return json
        except Exception as ex:
            logger.error("Invalid token : " + str(ex) + " " + payload)
            raise PermissionDenied("Invalid token")

    def post(self, request, *args, **kwargs):
        """
        [BACKEND ONLY] Push notification

        Receive: jwt token with embed json data

        {
            "iss": "user_id",
            "iat": 1300819370,
            "exp": 1300819380,
            "mth": "fb/gg/em/mo",
            "tok": "token",
            "sub": "subject_id",
            "context": {
                "notification": {
                "title": "title",
                "body": "body"
                },
                "data": {
                    ....
                },
                "priority": "xxx"
            },
            "admin": false
        }

        ---
        parameters:
            - name: payload
              description: token string
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"message" : "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid"}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure"}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found"}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error"}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        data = request.POST
        payload = data.get('payload')
        if not payload:
            raise PermissionDenied("Unauthorized")
        # Verify token & get payload data
        json_data = self.verify_token(payload)
        logger.info(json.dumps(json_data))
        try:
            message = json.loads(json_data['context'])
            logger.info(json.dumps(message))
            headers = {
                'Content-Type': 'application/json',
                'Authorization': 'key=AIzaSyCvFeZVPTe9yMoe8Eo-VAJnUdlJT7Odmk8'
            }
            params = {
                'registration_ids': [],
                'data': message['data'],
            }
            if 'priority' in message:
                params['priority'] = message['priority']

            if 'notification' in message:
                params['notification'] = message['notification']
            list_token = AccessToken.objects.filter(user_id=json_data['iss']).values('push_token').annotate(
                dcount=Count('push_token'))
            for token in list_token:
                if token['push_token'] != '':
                    params['registration_ids'].append(token['push_token'])
            logger.info(json.dumps(params))
            r = requests.post('https://fcm.googleapis.com/fcm/send', data=json.dumps(params), headers=headers)
            logger.info(r.content)
            return response.success({"status":"success"})
        except Exception as ex:
            logger.error(ex)
            return response.error("Push access token not found")