# -*- coding: utf-8 -*-
import re

from api.common import response
from cas.models.account import Account
from cas.models.access_token import AccessToken
from cas.models.email_account import EmailAccount
from cas.models.email_otp import EmailOtp
from cas.models.mobile_account import MobileAccount
from billing.services.subscription import add_user_subscription
from cas.authentication.token_authentication import TokenAuthentication
from cas.permissions.token_permission import TokenIsAuthenticated
from cas.serializers.account_serializer import AccountSerializer
from rest_framework.views import APIView
from rest_framework.generics import UpdateAPIView
from django.conf import settings
from django.template.loader import get_template
from django.template import Context
from django.core.mail import EmailMultiAlternatives
from django.contrib.auth.hashers import make_password, check_password
from api.utils import get_client_ip
import time, requests, random


class InitialPassword(APIView):
    def post(self, request, *args, **kwargs):

        """
        Initial password for email
        ---
        type:
              access_token:
                required: true
                type: string
              password:
                required: true
                type: string

        parameters:

            - name: authorization
              description: Access token
              required: true
              type: string
              paramType: header

            - name: password
              description: Password
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            access_token = request.META.get('HTTP_AUTHORIZATION', None)
            password = request.POST.get('password', None)

            if not access_token:
                return response.fail("Access token not found")

            access_token_obj = AccessToken.objects.get(id=access_token.strip(), status=1)
            user = access_token_obj.user
            account = None
            if user.email is not None:
                account = user.email
            elif user.mobile is not None:
                account = user.mobile
            else:
                return response.fail("Invalid account")

            if account.password is None or account.password == '':
                account.password = make_password(password.strip())
                account.save()
            else:
                return response.fail("Password existed")

        except Exception as ex:
            print ex.message
            return response.fail("Access token not found")

        return response.success({'status': 'success'})


class ChangePassword(APIView):
    def post(self, request, *args, **kwargs):

        """
        Change password for email
        ---
        type:
              access_token:
                required: true
                type: string
              password:
                required: true
                type: string
        parameters:

            - name: authorization
              description: Access token
              required: true
              type: string
              paramType: header

            - name: old_password
              description: Old password
              required: true
              type: string
              paramType: form

            - name: password
              description: Password
              required: true
              type: string
              paramType: form
        """
        try:
            access_token = request.META.get('HTTP_AUTHORIZATION', None)
            password = request.POST.get('password', '')
            old_password = request.POST.get('old_password', None)

            if not access_token:
                return response.fail("Access token not found")

            if not old_password or password.strip() == '':
                return response.fail("Password not found")

            access_token_obj = AccessToken.objects.get(id=access_token.strip())
            user = access_token_obj.user

            if user.email is not None and check_password(old_password.strip(), user.email.password.strip()):
                user.email.password = make_password(password.strip())
                user.email.save()
            else:
                return response.fail("Password is not match")

        except Exception as ex:
            print ex.message
            return response.fail("Access token not found")

        return response.success({'status': 'success'})


class ResetPassword(APIView):
    def post(self, request, *args, **kwargs):
        """
        Reset password using email
        ---
        type:
              access_token:
                required: true
                type: string
              password:
                required: true
                type: string

        parameters:

            - name: authorization
              description: Access token
              required: true
              type: string
              paramType: header

            - name: password
              description: Password
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            token = request.META.get('HTTP_AUTHORIZATION', None)
            password = request.POST.get('password', '')

            if password.strip() == '':
                return response.fail("Password invalid")

            access_token = AccessToken.objects.get(id=token.strip())

            if time.time() > access_token.expire:
                return response.fail('Token expired', {'token': 'expired'})

            local_account = access_token.user

            if local_account.email is None:
                return response.fail('Token invalid', {'token': 'invalid'})

            email_account = local_account.email
            email_account.password = make_password(password.strip())
            email_account.save()

            access_token.delete()

        except Exception as ex:
            print ex.message
            return response.fail('Token invalid', {'token': 'invalid'})

        return response.success({'status': 'success'})

    def put(self, request, *args, **kwargs):
        """
        Generate url for reset password by sending email
        ---
        type:
              email:
                required: true
                type: string

        parameters:

            - name: email
              description: Email
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        email = request.POST.get('email', None)

        if email is None:
            return response.fail("Email not found")

        try:
            email_obj = EmailAccount.objects.get(email=email.strip())
            local_account = Account.objects.get(email=email_obj)
            access_token = AccessToken(user=local_account, expire=int(time.time()) + 2 * 60 * 60)
            access_token.save()

            html_template = get_template('reset_password.html')
            txt_template = get_template('reset_password.txt')
            html_content = html_template.render(
                Context({'email': email.strip(), 'url': settings.RESET_PASSWORD_URL + '?token=' + access_token.id}))
            txt_content = txt_template.render(
                Context({'email': email.strip(), 'url': settings.RESET_PASSWORD_URL + '?token=' + access_token.id}))

            msg = EmailMultiAlternatives("Thiết lập mật khẩu", txt_content, "no-reply@mail.upbox.vn", [email.strip()])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        except Exception as ex:
            print ex.message
            return response.fail("Email does not exist.")

        return response.success({'status': 'success'})


class ResetPasswordOtp(APIView):
    def post(self, request, *args, **kwargs):
        """
        Reset password by OTP
        ---
        type:
          email:
            required: true
            type: string
        parameters:
          - name: email
            description: Email
            required: true
            type: string
            paramType: form
        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        try:
            email = request.POST.get('email', '')
            obj = EmailAccount.objects.get(email=email.strip())
            otp = EmailOtp(email=obj, otp='{:04d}'.format(random.randrange(0, 9999, 1)),
                           expire=int(time.time()) + 24 * 60 * 60)
            otp.save()

            html_template = get_template('email_otp_en.html')
            txt_template = get_template('email_otp_en.txt')
            html_content = html_template.render(Context({'code': otp.otp}))
            txt_content = txt_template.render(Context({'code': otp.otp}))

            msg = EmailMultiAlternatives("Reset password", txt_content, "no-reply@mail.upbox.vn", [email.strip()])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        except Exception as ex:
            print ex.message
            return response.fail("Email does not exist.")
        return response.success({'status': 'success'})

    def put(self, request, *args, **kwargs):
        """
        Validate OTP by email
        ---
        type:
          email:
            required: true
            type: string
          otp:
            required: true
            type: string
        parameters:
          - name: email
            description: Email
            required: true
            type: string
            paramType: form
          - name: otp
            description: OTP
            required: true
            type: string
            paramType: form
        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            email = request.POST.get('email', '')
            otp = request.POST.get('otp', '')

            obj = EmailAccount.objects.get(email=email.strip())
            obj1 = EmailOtp.objects.get(email=obj, otp=otp.strip(), status=1)

            if time.time() > obj1.expire:
                return response.fail('', {'otp_expired': 1})
            obj1.delete()

            local_account = Account.objects.get(email=obj)
            access_token = AccessToken(user=local_account, expire=12 * 60 * 60)
            access_token.save()
            return response.success({'access_token': access_token.id})
        except Exception as ex:
            print ex.message
            return response.fail('Email or OTP does not exist.', {'otp_invalid': 1})

        return response.fail("system errors")


class ResetPasswordEmailOtp(APIView):
    def post(self, request, *args, **kwargs):
        """
        OTP reset password by email
        ---
        type:
          email:
            required: true
            type: string

        parameters:
          - name: language
            description: vi / en
            required: false
            type: string
            paramType: form

          - name: email
            description: Email
            required: true
            type: string
            paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        lang = request.POST.get('language', 'vi')
        if lang != 'en':
            lang = 'vi'

        try:
            email = request.POST.get('email', '')
            obj = EmailAccount.objects.get(email=email.strip())
            try:
                EmailOtp.objects.filter(email=obj).delete()
            except:
                pass
            otp = EmailOtp(email=obj, otp='{:06d}'.format(random.randrange(100000, 999999, 1)),
                           expire=int(time.time()) + 2 * 60 * 60)
            otp.save()

            html_template = get_template('email_otp_reset_password_%s.html' % lang)
            txt_template = get_template('email_otp_reset_password_%s.txt' % lang)
            html_content = html_template.render(Context({'code': otp.otp}))
            txt_content = txt_template.render(Context({'code': otp.otp}))

            subject = re.search('<title>(.*)</title>', html_content, re.IGNORECASE)

            msg = EmailMultiAlternatives(subject.group(1), txt_content, "no-reply@mail.upbox.vn", [email.strip()])
            msg.attach_alternative(html_content, "text/html")
            msg.send()
        except Exception as ex:
            return response.fail("Email does not exist. " + ex.message)
        return response.success({'status': 'success'})

    def put(self, request, *args, **kwargs):
        """
        Check validate OTP reset password by email
        ---
        type:
          email:
            required: true
            type: string
          otp:
            required: true
            type: string
        parameters:
          - name: language
            description: vi / en
            required: false
            type: string
            paramType: form

          - name: email
            description: Email
            required: true
            type: string
            paramType: form

          - name: otp
            description: OTP
            required: true
            type: string
            paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        lang = request.POST.get('language', 'vi')
        if lang != 'en':
            lang = 'vi'

        try:
            email = request.POST.get('email', '')
            otp = request.POST.get('otp', '')

            obj = EmailAccount.objects.get(email=email.strip())
            obj1 = EmailOtp.objects.get(email=obj, otp=otp.strip(), status=1)

            if time.time() > obj1.expire:
                return response.fail('', {'otp_expired': 1})

            EmailOtp.objects.filter(email=obj).delete()

            local_account = Account.objects.get(email=obj)
            access_token = AccessToken(user=local_account, expire=int(time.time()) + 2 * 60 * 60)
            access_token.save()
            return response.success({'access_token': access_token.id})
        except Exception as ex:
            print ex.message
            return response.fail('Email or OTP does not exist.', {'otp_invalid': 1})


class Register(APIView):
    def post(self, request, *args, **kwargs):

        """
        Register new profile by email
        ---
        type:
              email:
                required: true
                type: string
              password:
                required: true
                type: string

        parameters:

            - name: email
              description: Email
              required: true
              type: string
              paramType: form

            - name: name
              description: Full name
              required: false
              type: string
              paramType: form

            - name: password
              description: Password
              required: true
              type: string
              paramType: form

            - name: g-recaptcha-response
              description: recaptcha
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            if len(args) > 0 and args[0] == 'web':
                url = "https://www.google.com/recaptcha/api/siteverify"
                g_captcha = request.POST.get('g-recaptcha-response', '')
                params = {
                    'secret': settings.RECAPTCHA_SECRET_KEY,
                    'response': g_captcha,
                    'remoteip': get_client_ip(request)
                }

                verify_rs = requests.get(url, params=params, verify=True)
                verify_rs = verify_rs.json()

                if not verify_rs.get("success", False):
                    return response.fail("Captcha invalid")

            email = request.POST.get('email', '')
            password = request.POST.get('password', '')
            full_name = request.POST.get('name', '')

            if email.strip() == '' or password.strip() == '':
                return response.fail("Email is null")

            if len(password) < 6 or password == email:
                return response.fail("Mật khẩu phải bao gồm ít nhất 6 ký tự và không được trùng với tên đăng nhập")

            email_account = EmailAccount.objects.filter(email=email.strip())

            if len(email_account) == 0:
                email_account = EmailAccount(email=email.strip(), status=0, password=make_password(password.strip()))
                email_account.save()
                local_account = Account(email=email_account, full_name=full_name)
                local_account.save()

                otp = EmailOtp(email=email_account, otp='{:04d}'.format(random.randrange(0, 9999, 1)),
                               expire=int(time.time()) + 24 * 60 * 60, status=1)
                otp.save()

                user_subscription, user_subscription_detail = \
                    add_user_subscription(local_account.id, None, default_storage=settings.DEFAULT_QUOTA_CONTAINER)
                user_subscription.save()
                user_subscription_detail.save()

                html_template = get_template('email_otp_en.html')
                txt_template = get_template('email_otp_en.txt')
                html_content = html_template.render(Context({'code': otp.otp}))
                txt_content = txt_template.render(Context({'code': otp.otp}))

                msg = EmailMultiAlternatives("Confirm your password", txt_content, "no-reply@mail.upbox.vn",
                                             [email.strip()])
                msg.attach_alternative(html_content, "text/html")
                msg.send()
            else:
                return response.fail("Email has already existed")
        except Exception as ex:
            print ex.message
            return response.fail(ex.message)

        return response.success({'status': 'success'})

    def put(self, request, *args, **kwargs):

        """
        Confirm email
        ---
        type:
              otp:
                required: true
                type: string
              email:
                required: true
                type: string

        parameters:

            - name: otp
              description: OTP
              required: true
              type: string
              paramType: form
            - name: email
              description: Email
              required: true
              type: string
              paramType: form


        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        try:
            email = request.POST.get('email', '')
            otp = request.POST.get('otp', '')

            obj = EmailAccount.objects.get(email=email.strip())
            obj1 = EmailOtp.objects.get(email=obj, otp=otp.strip(), status=1)

            if time.time() > obj1.expire:
                return response.fail('', {'otp_expired': 1})

            obj.status = 1
            obj.save()

            obj1.delete()

        except Exception as ex:
            print ex.message
            return response.fail(ex.message, {'otp_invalid': 1})

        return response.success({'status': 'success'})


class ManageProfileView(APIView):
    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)
    # queryset = Account.objects.all()
    serializer_class = AccountSerializer

    def get(self, request, *args, **kwargs):
        """
        Get profile
        ---
        type:
              profile:
                required: true
                type: json {id, name, first_name, last_name, email}
              access_token:
                required: false
                type: string

        parameters:

            - name: Accept-Language
              description: For response message in difference language  ( en / vi )
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: Access token
              required: true
              type: string
              paramType: header

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user = request.auth
        data = AccountSerializer(user)
        profile = data.data
        if user.email is not None:
            if user.email.password is None or user.email.password == '':
                profile['set_password'] = 1
            else:
                profile['set_password'] = 0
        return response.success(profile)

    def post(self, request, *args, **kwargs):
        """
        Update profile
        ---
        type:
              access_token:
                required: false
                type: string

        parameters:

            - name: Authorization
              description: Access token
              required: true
              type: string
              paramType: header

            - name: full_name
              description: full name
              required: false
              type: string
              paramType: form

            - name: dob
              description: date of birth (YYYY-MM-DD)
              required: false
              type: string
              paramType: form

            - name: gender
              description: choice (0-unknown , 1-male , 2-female)
              required: false
              type: integer
              paramType: choice

            - name: avatar
              description: path to avatar
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user = request.auth
        serializer = AccountSerializer(user, data=request.POST, partial=True)
        if serializer.is_valid(raise_exception=True):
            serializer.save()
            return response.success({'status': 'success'})
        else:
            return response.fail("Update fail")