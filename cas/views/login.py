from rest_framework.views import APIView
from cas.serializers.account_serializer import AccountSerializer
from cas.models import Account, AccessToken , EmailAccount, MobileAccount, Device
from billing.services.subscription import add_user_subscription
from api.common import response
from django.core.cache import cache
from django.contrib.auth.hashers import check_password
from jose import jwt
from django.conf import settings
import requests, json, uuid, time
import logging

logger = logging.getLogger(__name__)

class FacebookLogin(APIView):
    def post(self, request, *args, **kwargs):

        """
        Login facebook {set_password:0 / status:have a password, set_password:1 / status:null.}
        ---
        type:
              token:
                required: true
                type: string

        parameters:

            - name: token
              description: Account kit access token
              required: true
              type: string
              paramType: form

            - name: device_name
              description: device name
              required: false
              type: string
              paramType: form

            - name: os_version
              description: os version
              required: false
              type: string
              paramType: form

            - name: app_version
              description: app version
              required: false
              type: string
              paramType: form

            - name: push_token
              description: token for push notification
              required: false
              type: string
              paramType: form

            - name: platform
              description: platform
              required: false
              type: string
              paramType: form

            - name: ref_code
              description: ref_code is the value of uu_code of invite user
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        fb_token = request.POST.get('token', None)

        if not fb_token:
            return response.fail('Facebook token not found')

        device_name = request.POST.get('device_name', None)
        app_version = request.POST.get('app_version', None)
        os_version = request.POST.get('os_version', None)
        push_token = request.POST.get('push_token', None)
        platform = request.POST.get('platform', None)
        ref_code = request.POST.get('ref_code', None)

        try:
            url = 'https://graph.facebook.com/v2.8/me'
            profile = requests.get(url, params={'access_token': fb_token,
                                                'format': 'json',
                                                'method': 'get',
                                                'suppress_http_code': 1,
                                                'fields': 'name,first_name,email,last_name'})
            profile = json.loads(profile.text)

            if "error" in profile:
                logger.error(profile)
                return response.fail(profile['error'].get('message', ""))

            # Register
            email = profile['email']
            try:

                email_account = EmailAccount.objects.get(email=email)
                local_account = Account.objects.get(email=email_account)

                if email_account.facebook == '':
                  local_account.first_name = profile['first_name']
                  local_account.last_name = profile['last_name']
                  local_account.full_name = ' '.join((profile['first_name'], profile['last_name'])).strip()
                  local_account.avatar = "https://graph.facebook.com/" + profile['id'] + "/picture?type=large"
                  local_account.facebook = profile['id']
                  local_account.save()
            except Exception as ex:
                email_account = EmailAccount(email=profile['email'], facebook=profile['id'], status=1)
                email_account.save()
                local_account = Account(email=email_account,
                                        first_name=profile['first_name'],
                                        last_name=profile['last_name'],
                                        full_name=' '.join((profile['first_name'], profile['last_name'])).strip(),
                                        avatar="https://graph.facebook.com/" + profile['id'] + "/picture?type=large",
                                        ref_code=ref_code)
                local_account.save()
                user_subscription, user_subscription_detail = \
                    add_user_subscription(local_account.id, None, default_storage=settings.DEFAULT_QUOTA_CONTAINER)
                user_subscription.save()
                user_subscription_detail.save()

            set_password = 0
            if email_account.password is None or email_account.password == '':
                set_password = 1

            # Generate access token

            profile_data = AccountSerializer(local_account)
            access_token = AccessToken(user=local_account, expire=int(time.time())+24*60*60*30, app_version = None if app_version is None else app_version.strip(), push_token=None if push_token is None else push_token.strip())
            access_token.save()
            if device_name is not None or platform is not None or os_version is not None:
                device = Device(token=access_token, name=None if device_name is None else device_name.strip(), platform=None if platform is None else platform.strip(), os=None if os_version is None else os_version.strip())
                device.save()

            #SYNC TOKEN

            token_data = {'iss':access_token.user.id, 'iat':int(time.mktime(access_token.created_at.timetuple())),'exp':access_token.expire, 'mth':'fb', 'tok':access_token.id, 'sub':access_token.subject_id,  'context':profile_data.data, 'admin':False }

            token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')

            rs = requests.post(settings.BLOCK_URL+'/private/sync_token/', data={'payload':token})

            result = {'profile': profile_data.data, 'access_token': access_token.id, 'subject_id':access_token.subject_id, 'set_password':set_password}

            # CACHE FACEBOOK INFORMATION
            cache.set('facebook_'+str(access_token.id), profile_data.data)
        except Exception as ex:
            logger.error(ex)
            return response.fail("Internal Error. Please try again later.")
        return response.success(result)

class GoogleLogin(APIView):
    def post(self, request, *args, **kwargs):

        """
        Login google {set_password: 0 / status:have a password, set_password: 1 / status:null.}
        ---
        type:
              token:
                required: true
                type: string

        parameters:

            - name: token
              description: Account kit access token
              required: true
              type: string
              paramType: form

            - name: device_name
              description: device name
              required: false
              type: string
              paramType: form

            - name: os_version
              description: os version
              required: false
              type: string
              paramType: form

            - name: app_version
              description: app version
              required: false
              type: string
              paramType: form

            - name: push_token
              description: token for push notification ( firebase )
              required: false
              type: string
              paramType: form

            - name: platform
              description: platform
              required: false
              type: string
              paramType: form
            
            - name: ref_code
              description: ref_code is the value of uu_code of invite user
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

        token = request.POST.get('token', None)

        if not token:
            return response.fail('Google token not found')

        device_name = request.POST.get('device_name', None)
        app_version = request.POST.get('app_version', None)
        os_version = request.POST.get('os_version', None)
        push_token = request.POST.get('push_token', None)
        platform = request.POST.get('platform', None)
        ref_code = request.POST.get('ref_code', None)

        try:
            url = 'https://www.googleapis.com/oauth2/v3/tokeninfo'
            profile = requests.get(url, params={'id_token': token,}, timeout=30)

            profile = json.loads(profile.text)

            if "error" in profile or not('email' in profile):
                return response.fail(profile['error'].get('message', ""))

            family_name = profile['family_name'] if 'family_name' in profile else ''
            given_name = profile['given_name'] if 'given_name' in profile else ''

            try:
                email_account = EmailAccount.objects.get(email=profile['email'])
                local_account = Account.objects.get(email=email_account)

                if email_account.google == '':
                  local_account.first_name = profile['family_name'] if 'family_name' in profile else None
                  local_account.last_name = profile['given_name'] if 'given_name' in profile else None
                  local_account.full_name = ' '.join((family_name, given_name)).strip()
                  local_account.avatar = profile['picture'] if 'picture' in profile else None
                  local_account.google = profile['id'] if 'id' in profile else '1'
                  local_account.save()
            except Exception as ex:
                email_account = EmailAccount(email=profile['email'], google= profile['id'] if 'id' in profile else '1', status=1)
                email_account.save()
                family_name = profile['family_name'] if 'family_name' in profile else ''
                given_name = profile['given_name'] if 'given_name' in profile else ''
                local_account = Account(email=email_account,
                                        first_name=profile['family_name'] if 'family_name' in profile else None,
                                        last_name=profile['given_name'] if 'given_name' in profile else None,
                                        full_name=' '.join((family_name, given_name)).strip(),
                                        avatar=profile['picture'] if 'picture' in profile else None,
                                        ref_code=ref_code)
                local_account.save()
                user_subscription, user_subscription_detail = \
                    add_user_subscription(local_account.id, None, default_storage=settings.DEFAULT_QUOTA_CONTAINER)
                user_subscription.save()
                user_subscription_detail.save()

            set_password = 0
            if email_account.password is None or email_account.password == '':
                set_password = 1

            # Generate access token
            profile_data = AccountSerializer(local_account)

            access_token = AccessToken(user=local_account, expire=int(time.time())+24*60*60*30, app_version = None if app_version is None else app_version.strip(), push_token=None if push_token is None else push_token.strip())
            access_token.save()
            if device_name is not None or platform is not None or os_version is not None:
                device = Device(token=access_token, name=None if device_name is None else device_name.strip(), platform=None if platform is None else platform.strip(), os=None if os_version is None else os_version.strip())
                device.save()

            #SYNC TOKEN
            token_data = {'iss':access_token.user.id, 'iat':int(time.mktime(access_token.created_at.timetuple())),'exp':access_token.expire, 'mth':'gg', 'tok':access_token.id, 'sub':access_token.subject_id,  'context':profile_data.data, 'admin':False }

            token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')

            rs = requests.post(settings.BLOCK_URL+'/private/sync_token/', data={'payload':token})

            result = {'profile': profile_data.data, 'access_token': access_token.id, 'subject_id':access_token.subject_id, 'set_password':set_password}

            # CACHE FACEBOOK INFORMATION
            cache.set('google_' + str(access_token.id), profile_data.data)

        except Exception as ex:
            print ex.message
            return response.fail("System Error")
        return response.success(result)

class CheckEmail(APIView):

  def post(self, request, *args, **kwargs):
    """
        Check exist for email login
        ---
        type:
              email:
                required: true
                type: string

        parameters:
            - name: email
              description: Email
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

    try:
      obj = EmailAccount.objects.get(email=request.POST.get('email',''))

      if obj.status == 0:
        return response.success({'exist':2})
        
    except Exception as ex:
      return response.success({'exist':0})

    return response.success({'exist':1})

class EmailLogin(APIView):    

  def post(self, request, *args, **kwargs):
    """
      Login by email
      ---
      type:
            email:
              required: true
              type: string
            password:
              required: true
              type: string

      parameters:
          - name: email
            description: Email
            required: true
            type: string
            paramType: form

          - name: password
            description: Password
            required: true
            type: string
            paramType: form

          - name: device_name
            description: device name
            required: false
            type: string
            paramType: form

          - name: os_version
            description: os version
            required: false
            type: string
            paramType: form

          - name: app_version
            description: app version
            required: false
            type: string
            paramType: form

          - name: push_token
            description: token for push notification
            required: false
            type: string
            paramType: form

          - name: platform
            description: platform
            required: false
            type: string
            paramType: form

          - name: ref_code
            description: ref_code is the value of uu_code of invite user
            required: false
            type: string
            paramType: form

      responseMessages:
          - code: 200
            message: Success
          - code: 400
            message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
          - code: 403
            message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
          - code: 404
            message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
          - code: 500
            message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

      consumes:
          - application/json

      produces:
          - application/json
      """
    try:
      email = request.POST.get('email', None)
      password = request.POST.get('password', None)
      email_account = EmailAccount.objects.get(email=email.strip())

      if email_account.status == 0:
        return response.fail('email is not active', data={'confirm':0})

      if not check_password(password.strip(), email_account.password.strip()):
        return response.fail('Cannot Log In - Please check that your email and password are correct')
    except Exception as ex:
      return response.fail('Cannot Log In - Please check that your email and password are correct')

    device_name = request.POST.get('device_name', None)
    app_version = request.POST.get('app_version', None)
    os_version = request.POST.get('os_version', None)
    push_token = request.POST.get('push_token', None)
    platform = request.POST.get('platform', None)
    ref_code = request.POST.get('ref_code', None)

    try:
      local_account = Account.objects.get(email=email_account)
    except Exception as ex:
      local_account = Account(email=email_account, first_name=None, last_name=None, avatar=None,
                              ref_code=ref_code)
      local_account.save()
      user_subscription, user_subscription_detail = \
                    add_user_subscription(local_account.id, None, default_storage=settings.DEFAULT_QUOTA_CONTAINER)
      user_subscription.save()
      user_subscription_detail.save()

    profile_data = AccountSerializer(local_account)
    access_token = AccessToken(user=local_account, expire=int(time.time())+24*60*60*30, app_version = None if app_version is None else app_version.strip(), push_token=None if push_token is None else push_token.strip())
    access_token.save()
    if device_name is not None or platform is not None or os_version is not None:
        device = Device(token=access_token, name=None if device_name is None else device_name.strip(), platform=None if platform is None else platform.strip(), os=None if os_version is None else os_version.strip())
        device.save()

    #SYNC TOKEN
    mth='em'
    if not(local_account.email.facebook is None):
      mth='fb'
    elif not(local_account.email.google is None):
      mth='gg'

    token_data = {'iss':access_token.user.id, 'iat':int(time.mktime(access_token.created_at.timetuple())),'exp':access_token.expire, 'mth':mth, 'tok':access_token.id, 'sub':access_token.subject_id,  'context':profile_data.data, 'admin':False }

    token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')

    rs = requests.post(settings.BLOCK_URL+'/private/sync_token/', data={'payload':token})

    result = {'profile': profile_data.data, 'access_token': access_token.id, 'subject_id':access_token.subject_id}

    # CACHE EMAIL INFORMATION
    cache.set('email_' + str(access_token.id), profile_data.data)

    return response.success(result)

class CheckMobile(APIView):
  def post(self, request, *args, **kwargs):
    """
        Check exist for mobile
        ---
        type:
              phone:
                required: true
                type: string

        parameters:
            - name: phone
              description: Phone
              required: true
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """

    try:
      MobileAccount.objects.get(phone=request.POST.get('phone',''))
    except Exception as ex:
      return response.success({'exist':0})

    return response.success({'exist':1})

class MobileLogin(APIView):    

  def put(self, request, *args, **kwargs):
    """
      Login by mobile
      ---
      type:
            phone:
              required: true
              type: string
            password:
              required: true
              type: string

      parameters:
          - name: phone
            description: Phone
            required: true
            type: string
            paramType: form
          - name: password
            description: Password
            required: true
            type: string
            paramType: form
          - name: ref_code
            description: ref_code is the value of uu_code of invite user
            required: false
            type: string
            paramType: form

      responseMessages:
          - code: 200
            message: Success
          - code: 400
            message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
          - code: 403
            message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
          - code: 404
            message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
          - code: 500
            message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

      consumes:
          - application/json

      produces:
          - application/json
      """
    try:
      phone = request.POST.get('phone', None)
      password = request.POST.get('password', '')
      mobile_account = MobileAccount.objects.get(phone=phone.strip())
      ref_code = request.POST.get('ref_code', None)

      if not check_password(password.strip(), mobile_account.password.strip()):
        return response.fail('Cannot Log In - Please check that your email and password are correct')
    except Exception as ex:
      return response.fail('Cannot Log In - Please check that your email and password are correct')

    try:
      local_account = Account.objects.get(mobile=mobile_account)
    except Exception as ex:
      local_account = Account(mobile=mobile_account, first_name=None, last_name=None, avatar=None, ref_code=ref_code)
      local_account.save()
      user_subscription, user_subscription_detail = \
                    add_user_subscription(local_account.id, None, default_storage=settings.DEFAULT_QUOTA_CONTAINER)
      user_subscription.save()
      user_subscription_detail.save()

    profile_data = AccountSerializer(local_account)
    access_token = AccessToken(user=local_account, expire=int(time.time())+24*60*60*30)
    access_token.save()

    #SYNC TOKEN
    token_data = {'iss':access_token.user.id, 'iat':int(time.mktime(access_token.created_at.timetuple())),'exp':access_token.expire, 'mth':'mo', 'tok':access_token.id, 'sub':access_token.subject_id,  'context':profile_data.data, 'admin':False }

    token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')

    rs = requests.post(settings.BLOCK_URL+'/private/sync_token/', data={'payload':token})

    result = {'profile': profile_data.data, 'access_token': access_token.id, 'subject_id':access_token.subject_id}

    # CACHE EMAIL INFORMATION
    cache.set('mobile_' + str(access_token.id), profile_data.data)

    return response.success(result)


  def post(self, request, *args, **kwargs):

    """
    Authentication for mobile
    ---
    type:
          token:
            required: true
            type: string

    parameters:
        - name: token
          description: Account kit access token
          required: true
          type: string
          paramType: form

        - name: device_name
          description: device name
          required: false
          type: string
          paramType: form

        - name: os_version
          description: os version
          required: false
          type: string
          paramType: form

        - name: app_version
          description: app version
          required: false
          type: string
          paramType: form

        - name: push_token
          description: token for push notification
          required: false
          type: string
          paramType: form

        - name: platform
          description: platform
          required: false
          type: string
          paramType: form

        - name: ref_code
          description: ref_code is the value of uu_code of invite user
          required: false
          type: string
          paramType: form

    responseMessages:
        - code: 200
          message: Success
        - code: 400
          message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
        - code: 403
          message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
        - code: 404
          message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
        - code: 500
          message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

    consumes:
        - application/json

    produces:
        - application/json
    """

    try:
      token = request.POST.get('token', None)

      if token is None:
        return response.fail('Token not found')

      device_name = request.POST.get('device_name', None)
      app_version = request.POST.get('app_version', None)
      os_version = request.POST.get('os_version', None)
      push_token = request.POST.get('push_token', None)
      platform = request.POST.get('platform', None)
      ref_code = request.POST.get('ref_code', None)

      url = 'https://graph.accountkit.com/v1.0/access_token'

      result = requests.get(url, params={'grant_type': 'authorization_code', 'code':token, 'access_token':'|'.join(('AA', settings.FACEBOOK_APP_ID, settings.FACEBOOK_APP_SECRET))})

      result = json.loads(result.text)

      if 'error' in result:
        return response.fail(result['error']['message'])

      url = 'https://graph.accountkit.com/v1.0/me'

      result = requests.get(url, params={'access_token':result['access_token']}, timeout=30)

      result = json.loads(result.text)

      if 'error' in result:
        return response.fail(result['error']['message'])

      try:
        mobile_account = MobileAccount.objects.get(phone=result['phone']['number'])
        local_account = Account.objects.get(mobile=mobile_account)
      except Exception as ex1:
        mobile_account = MobileAccount(phone=result['phone']['number'], country_prefix=result['phone']['country_prefix'], national_number=result['phone']['national_number'])
        mobile_account.save()
        local_account = Account(mobile=mobile_account, ref_code=ref_code)
        local_account.save()
        user_subscription, user_subscription_detail = \
                    add_user_subscription(local_account.id, None, default_storage=settings.DEFAULT_QUOTA_CONTAINER)
        user_subscription.save()
        user_subscription_detail.save()

      set_password = 0
      if mobile_account.password is None or mobile_account.password == '':
        set_password = 1

      profile_data = AccountSerializer(local_account)

      access_token = AccessToken(user=local_account, expire=int(time.time())+24*60*60*30, app_version = None if app_version is None else app_version.strip(), push_token=None if push_token is None else push_token.strip())
      access_token.save()
      if device_name is not None or platform is not None or os_version is not None:
        device = Device(token=access_token, name=None if device_name is None else device_name.strip(), platform=None if platform is None else platform.strip(), os=None if os_version is None else os_version.strip())
        device.save()

      #SYNC TOKEN
      token_data = {'iss':access_token.user.id, 'iat':int(time.mktime(access_token.created_at.timetuple())),'exp':access_token.expire, 'mth':'mo', 'tok':access_token.id, 'sub':access_token.subject_id,  'context':profile_data.data, 'admin':False }

      token = jwt.encode(token_data, settings.JWT_PRIVATE_SIGNATURE, algorithm='HS256')

      rs = requests.post(settings.BLOCK_URL+'/private/sync_token/', data={'payload':token})

      result = {'profile': profile_data.data, 'access_token': access_token.id, 'subject_id':access_token.subject_id, 'set_password':set_password, 'sms_quota': 1}

      # CACHE FACEBOOK INFORMATION
      cache.set('mobile_' + str(access_token.id), profile_data.data)

      return response.success(result)

    except Exception as ex:
      print ex.message
      return response.fail("System errors")