from rest_framework import authentication
from rest_framework import exceptions
from cas.models.access_token import AccessToken

class TokenAuthentication(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION')
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token, status=1)
                except:
                    raise exceptions.AuthenticationFailed('Token not found')
                return (token.user_id, token.user)
            else:
                raise exceptions.AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)


class TokenAuthenticationGet(authentication.BaseAuthentication):

    def authenticate(self, request):
        token = request.GET.get('auth')
        try:
            if token:
                try:
                    token = AccessToken.objects.get(id=token, status=1)
                except:
                    raise exceptions.AuthenticationFailed('Token not found')
                return (token.user_id, token.user)
            else:
                raise exceptions.AuthenticationFailed('No such user')
        except Exception as ex:
            return (None, None)