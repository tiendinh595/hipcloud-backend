from cas.views import login, profile, logout, pages, help, feedback, private
from django.conf.urls import url

urlpatterns = [
    url(r'^login/facebook$', login.FacebookLogin.as_view()),
    url(r'^profile', profile.ManageProfileView.as_view()),
    url(r'^register?$', profile.Register.as_view()),
    url(r'^register/(\w*)?$', profile.Register.as_view()),
    url(r'^password/initial', profile.InitialPassword.as_view()),
    url(r'^password/reset/otp$', profile.ResetPasswordOtp.as_view()),
    url(r'^password/reset/email_otp$', profile.ResetPasswordEmailOtp.as_view()),
    url(r'^password/reset$', profile.ResetPassword.as_view()),
    url(r'^password/change', profile.ChangePassword.as_view()),
    url(r'^check/email', login.CheckEmail.as_view()),
    url(r'^check/mobile', login.CheckMobile.as_view()),
    url(r'^login/email', login.EmailLogin.as_view()),
    url(r'^login/google$', login.GoogleLogin.as_view()),
    url(r'^login/mobile$', login.MobileLogin.as_view()),
    url(r'^logout$', logout.Logout.as_view()),
    url(r'^pages/new_password', pages.new_password, name='new_password'),
    url(r'^info/help', help.HelpInfoView.as_view()),
    url(r'^info/feedback', feedback.FeedbackView.as_view()),

    url(r'^private/push_notification/', private.PrivatePushNotificationView.as_view()),

]