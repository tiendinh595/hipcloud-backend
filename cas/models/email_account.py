from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify

# Menu Model
class EmailAccount(models.Model):
    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    email = models.EmailField(blank = True, null = True, unique = True)
    password = models.CharField(max_length  = 256, blank = True, null = True)
    last_login = models.DateTimeField(blank=True, null=True, auto_now = True)
    facebook = models.CharField(max_length=50, blank = True, null = True)
    google = models.CharField(max_length=50, blank = True, null = True)
    created_at = models.DateTimeField(blank = True, null=True, auto_now_add = True)
    updated_at = models.DateTimeField(blank = True, null=True, auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 0)

    class Meta:
        db_table = 'cas_email_account'
