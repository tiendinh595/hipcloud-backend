#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify
from cas.models.account import Account

# Menu Model
class FeedBack(models.Model):

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    content = models.TextField(blank = True, null = True)
    created_at = models.DateTimeField(blank = True, null=True, auto_now_add = True)
    updated_at = models.DateTimeField(blank = True, null=True, auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_feedback'
