from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify
from cas.models.account import Account

# Menu Model


class AccessToken(models.Model):
    id = models.CharField(max_length = 64, primary_key = True, default = pkgen)
    user = models.ForeignKey(Account, blank = True, null = True )

    ip = models.CharField(max_length = 65,blank = True, null = True)
    app_version = models.CharField(max_length = 150,blank = True, null = True)
    push_token = models.CharField(max_length = 150,blank = True, null = True)
    subject_id = models.CharField(max_length = 150, blank=True, null=True, default = pkgen)
    expire = models.IntegerField(blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_access_token'