from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen
from cas.models.email_account import EmailAccount

# Menu Model
class EmailOtp(models.Model):
    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    email = models.ForeignKey(EmailAccount, blank = True, null = True , on_delete=models.SET_NULL)
    otp = models.CharField(max_length=36, blank = True, null = True)
    expire = models.IntegerField(blank=True, null=True)
    created_at = models.DateTimeField(blank = True, null=True, auto_now_add = True)
    updated_at = models.DateTimeField(blank = True, null=True, auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_email_otp'
