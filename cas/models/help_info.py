#!/usr/bin/env python
# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify

# Menu Model
class HelpInfo(models.Model):

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    question = models.TextField(blank = True, null = True)
    detail = models.TextField(blank = True, null = True)
    created_at = models.DateTimeField(blank = True, null=True, auto_now_add = True)
    updated_at = models.DateTimeField(blank = True, null=True, auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_help_info'
