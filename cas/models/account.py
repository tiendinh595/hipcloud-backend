import logging
from django.db import models, transaction
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify
from django.db.models.signals import post_save
from django.dispatch import receiver

# Menu Model
from cas.models.email_account import EmailAccount
from cas.models.mobile_account import MobileAccount

logger = logging.getLogger(__name__)


class Account(models.Model):
    GENDER_CHOICE = (
        (0, "Unknown") , (1, "Male"), (2, "Female"),
    )

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)

    email = models.ForeignKey(EmailAccount, blank = True, null = True , on_delete=models.SET_NULL)
    mobile = models.ForeignKey(MobileAccount, blank = True, null = True , on_delete=models.SET_NULL)

    first_name = models.CharField(max_length = 100, blank = True, null = True)
    last_name = models.CharField(max_length = 100, blank = True, null = True)
    full_name = models.CharField(max_length = 200, blank = True, null = True)
    dob = models.DateField(blank=True, null = True)
    gender = models.IntegerField(blank=True, null = True, default=0, choices=GENDER_CHOICE)
    address = models.CharField(max_length=100, blank=True, null=True)
    avatar = models.CharField(max_length=150, blank=True, null=True, default="default.jpg")

    banned_to = models.DateTimeField(blank = True, null = True)
    ref_code = models.CharField(max_length=36, default=None, null=True)
    uu_code = models.CharField(max_length=36, default = uugen, db_index=True)
    created_at = models.DateTimeField(blank=True, null = True, auto_now_add = True)
    updated_at = models.DateTimeField(blank=True, null = True, auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_account'

    def get_email(self):
        if self.facebook:
            return self.facebook.email
        if self.email:
            return self.email.email
        return ""


class AccountRefer(models.Model):

    id = models.CharField(max_length=36, primary_key=True, default=pkgen)
    user_id = models.CharField(max_length = 36, null=False, default=None)
    uu_code = models.CharField(max_length=36, null=False, default=None)
    invited_by_user_id = models.CharField(max_length = 36, null=False, default=None)
    created_at = models.DateTimeField(blank=True, null=True, auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True, auto_now=True)

    class Meta:
        db_table = 'cas_account_refer'
        unique_together = ('invited_by_user_id', 'user_id')


@receiver(post_save, sender=Account)
def update_stock(sender, instance, created, **kwargs):
    from billing.services.block import sync_container_info
    from billing.services.subscription import bonus_account_refer

    if created:
        try:
            results = bonus_account_refer(instance)
            if results:
                account_refer, invited_by_user_subscription, invited_by_user_subscription_detail = results
                with transaction.atomic():
                    account_refer.save()
                    invited_by_user_subscription.save()
                    invited_by_user_subscription_detail.save()
                    sync_response = sync_container_info(invited_by_user_subscription.user_id,
                                                        invited_by_user_subscription.storage)
        except Exception as e:
            import traceback
            traceback.print_exc()
            logger.exception(e)
