from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify

# Menu Model
from cas.models.access_token import AccessToken


class Device(models.Model):
    id = models.CharField(max_length=36, primary_key=True, default=pkgen)
    token = models.ForeignKey(AccessToken, blank = False, null = False , db_constraint = False)

    name = models.CharField(max_length = 100, blank = True, null = True)
    platform = models.CharField(max_length = 100, blank = True, null = True)
    os = models.CharField(max_length = 100, blank = True, null = True)
    carrier = models.CharField(max_length = 100, blank = True, null = True)
    model = models.CharField(max_length = 100, blank = True, null = True)
    series = models.CharField(max_length = 100, blank = True, null = True)
    open_id = models.CharField(max_length = 100, blank = True, null = True)
    mac_address = models.CharField(max_length = 64, blank = True, null = True)
    ip_address = models.CharField(max_length = 129, blank=True, null=True)

    created_at = models.DateTimeField(auto_now_add = True)
    updated_at = models.DateTimeField(auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_device'