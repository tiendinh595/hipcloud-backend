from __future__ import unicode_literals

from django.db import models
from api.common.strings import pkgen , uugen
from django.template.defaultfilters import slugify

# Menu Model
class MobileAccount(models.Model):
    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    phone = models.CharField(max_length = 100 ,blank = False, null = False, unique = True)
    country_prefix = models.CharField(max_length = 4 ,blank = True, null = True)
    national_number = models.CharField(max_length = 20 ,blank = True, null = True)
    password = models.CharField(max_length  = 256, blank = True, null = True)

    last_login = models.DateTimeField(blank=True, null=True,auto_now = True)
    created_at = models.DateTimeField(blank = True, null=True,auto_now_add = True)
    updated_at = models.DateTimeField(blank = True, null=True,auto_now = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'cas_mobile_account'