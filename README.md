READ ME 0.0.1

Backend is a project focus on user profile, authentication, authorization

Local Linux
======
## Elasticsearch
*  Open new terminal
Run elasticsearch
```
docker pull elasticsearch
docker run -d -p 9200:9200 elasticsearch
```
Check elasticsearch running
```
http://localhost:9200/
```
## Redis
*  Open new terminal
```
docker pull redis
docker run --name redis -p 6379:6379 -d redis
```
Check redis running
```
terminal% docker ps

Output :
05d55f4843e1        redis               "docker-entrypoint.sh"   3 hours ago         Up 3 hours          6379/tcp                                                   tender_mahavira
```
## Application

*  Open terminal
1. Setup python 2.7.x , to check python available.
```
terminal% which python
/usr/local/bin/python
```
2. Install linux dependencies 
```
apt-get update 
apt-get install -y supervisor 
apt-get install -y libmysqlclient-dev 
apt-get install -y build-essential 
apt-get install -y libssl-dev 
apt-get install -y libffi-dev 
```
3. Install pip : to check pip available:
```
teminal% which pip
Output:
/usr/local/bin/pip
```
4. Install virtualenv :
```
pip install virtualenv
```
4. Check virtualenv available :
```
terminal% which virtualenv
Output:
/usr/local/bin/virtualenv
```
5. Create virtual environment name *backend_venv* with virualenv:
```
cd folder_to_store_virtualenv
Output:
virtualenv backend_venv
```
6. Create virtual environment name *backend_venv* with virualenv:
```
cd folder_to_store_virtualenv
virtualenv backend_venv
```
7. Activate virualenv and a (backend_venv) will appear in the next line:
```
terminal% source $PATH/folder_to_store_virtualenv/backend_venv/bin/activate
Output:
(backend_venv)terminal%
```
8. Go to project directory:
```
(backend_venv)terminal% cd $PROJECT_DIR
```
9. Call install dependencies, file requirements.txt container in project root:
```
(backend_venv)terminal% pip install -r requirements.txt
```
10. Create db name backend_storage in mysql with UTF8 , UTF8_general_ci
11. Update local.py to connect redis and mysql
```
(backend_venv)terminal% vim $PATH/hipcloud_backend/settings/local.py
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'backend_storage',
        'USER': '<local-username-mysql>',
        'PASSWORD': '<local-password-mysql>',
        'HOST': '127.0.0.1',
        'PORT': '3306',
    }
}
CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://<redis-host>:<redis-port>/1",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
            "CONNECTION_POOL_KWARGS": {"max_connections": 200},
            "SOCKET_CONNECT_TIMEOUT": 5,  # in seconds
            "SOCKET_TIMEOUT": 5,  # in seconds
        }
    }
}
```
12. Makemigrations to create sync db structure with mysql
```
(backend_venv)terminal% python $PROJECT_ROOT/manage.py makemigrations \
--settings=hipcloud_block_storage.settings.local
```
13. Migrate to confirm sync db structure to mysql
```
(backend_venv)terminal% python $PROJECT_ROOT/manage.py migrate \
--settings=hipcloud_block_storage.settings.local
```
14. Collectstatic to prepapre static files for project
```
(backend_venv)terminal% python $PROJECT_ROOT/manage.py collectstatic \
--settings=hipcloud_block_storage.settings.local
```
15. Run Server at  *port* ( 8001 )
```
(backend_venv)terminal% python $PROJECT_ROOT/manage.py runserver 8001 \
--settings=hipcloud_block_storage.settings.local 

Output:

System check identified no issues (0 silenced).
February 21, 2017 - 03:52:48
Django version 1.8.17, using settings 'hipcloud_backend.settings.local'
Starting development server at http://127.0.0.1:8001/
Quit the server with CONTROL-C.
```


Docker
======


## Run

### Prod
```
docker build -t backend_storage:$BUILD_NUMBER -f Dockerfile .
docker stop backend_storage
docker rm backend_storage
docker run -d --name backend_storage -p 127.0.0.1:8801:8000 --link redis:redis -v /var/www/backend_storage-data/cas/migrations:/home/backend/cas/migrations -v /var/www/backend_storage-data/billing/migrations:/home/backend/billing/migrations -m 4096M --memory-swap -1 backend_storage:$BUILD_NUMBER
docker stop backend_storage_1
docker rm backend_storage_1
docker run -d --name backend_storage_1 -p 127.0.0.1:8901:8000 --link redis:redis -v /var/www/backend_storage-data/cas/migrations:/home/backend/cas/migrations -v /var/www/backend_storage-data/billing/migrations:/home/backend/billing/migrations -m 4096M --memory-swap -1 backend_storage:$BUILD_NUMBER
```
### Local



## Test

### Local Env
```
python manage.py test_coverage
python manage.py runserver --settings=hipcloud_backend.settings.local
```
### Prod Env
```
python manage.py test_coverage
python manage.py runserver --settings=hipcloud_backend.settings.prod
```
