#!/usr/bin/env python
# -*- coding: utf-8 -*-
from celery import shared_task
from django.db.models import Q
from django.db import transaction
from django.utils import timezone
import logging
from billing.models import Subscription, SubscriptionDetail
from billing.services.block import sync_container_info

logger = logging.getLogger(__name__)


@shared_task(bind=True)
def check_subscription(self, params):
    current_time = timezone.now()
    print('Start task check_subscription at: ' + str(current_time))
    condition = Q(status=SubscriptionDetail.ACTIVE, expiry_date__lte=current_time)
    query = SubscriptionDetail.objects.filter(condition)
    results = {'total': 0, 'success': 0}
    for sub_detail in query:
        try:
            # update subscription detail
            sub_detail.status = SubscriptionDetail.EXPIRED
            # update subscription
            subscription = Subscription.objects.get(user=sub_detail.user)
            subscription.storage -= sub_detail.storage
            with transaction.atomic():
                sub_detail.save()
                subscription.save()
                sync_response = sync_container_info(subscription.user_id, subscription.storage)
            results['success'] += 1
        except Exception as e:
            logger.exception(e)
        finally:
            results['total'] += 1
    print('Results: ' + str(results))
    return results
