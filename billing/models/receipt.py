#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from cas.models.account import Account
from billing.models.common import BaseModel, pkgen

class FingerPrint(BaseModel):
    ACTIVE = 1
    DEACTIVATE = 0
    STATUS_TYPES = (
        (ACTIVE, 1),
        (DEACTIVATE, 0)
    )

    id = models.CharField(max_length=36, primary_key=True, default=pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=True)
    # Store fingerprint of credit-card
    finger_print = models.CharField(max_length=65, blank=True, null=True)
    receipt_data = models.TextField(blank=True, null=True)
    receipt_response = models.TextField(blank=True, null=True)
    # Active / Disable
    status = models.IntegerField(choices=STATUS_TYPES, null=True, blank=True, default=ACTIVE)


    class Meta:
        abstract = True

class AndroidReceipt(BaseModel):
    PAYMENT_PENDING = 0
    PAYMENT_RECEIVED = 1
    PAYMENT_STATE_CHOICES = (
        (PAYMENT_PENDING, 0),
        (PAYMENT_RECEIVED, 1)
    )

    USER_CANCEL = 0
    SYSTEM_CANCEL = 1
    CANCEL_STATE_CHOICES = (
        (USER_CANCEL, 0),
        (SYSTEM_CANCEL, 1)
    )

    ACTIVE = 1
    DEACTIVATE = 0
    STATUS_TYPES = (
        (ACTIVE, 1),
        (DEACTIVATE, 0)
    )

    PURCHASED = 0
    CANCELED = 1
    REFUNDED = 2
    PURCHASE_STATE = (
        (PURCHASED, 0),
        (CANCELED, 1),
        (REFUNDED, 2)
    )

    NOT_CONSUMED = 0
    CONSUMED = 1
    CONSUMPTION_STATE = (
        (NOT_CONSUMED, 0),
        (CONSUMED, 1)
    )

    CONSUMPTION_KIND = "androidpublisher#productPurchase"
    SUBSCRIPTION_KIND = "androidpublisher#subscriptionPurchase"
    RECEIPT_KIND = (
        (CONSUMPTION_KIND, "androidpublisher#productPurchase"),
        (SUBSCRIPTION_KIND, "androidpublisher#subscriptionPurchase")
    )

    # def __init__(self, *args, **kwargs):
    #     super(AndroidReceipt, self).__init__(*args, **kwargs)
    #     if self.kind == AndroidReceipt.CONSUMPTION_KIND:
    #         self.__class__ = AndroidConsumableReceipt
    #     elif self.tykindpe == AndroidReceipt.SUBSCRIPTION_KIND:
    #         self.__class__ = AndroidSubscriptionReceipt


    id = models.CharField(max_length=36, primary_key=True, default=pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    kind = models.CharField(max_length=100 ,blank=True, null=True, choices=RECEIPT_KIND)
    token = models.CharField(max_length=250 ,blank=True, null=True, unique=True)
    start_time_millis = models.BigIntegerField(blank=True, null=True)
    expiry_time_millis = models.BigIntegerField(blank=True, null=True)
    auto_renewing = models.NullBooleanField(blank=True, null=True)
    price_currency_code = models.CharField(max_length=4 ,blank=True, null=True)
    price_amount_micros = models.BigIntegerField(blank=True, null=True)
    country_code = models.CharField(max_length=5, blank=True, null=True)
    developer_payload = models.TextField(blank=True, null=True)
    payment_state = models.IntegerField(blank=True, null=True, choices=PAYMENT_STATE_CHOICES)
    cancel_reason = models.IntegerField(blank=True, null=True, choices=CANCEL_STATE_CHOICES)
    purchase_state = models.IntegerField(blank=True, null=True, choices=PURCHASE_STATE)
    consumption_state = models.IntegerField(blank=True, null=True, choices=CONSUMPTION_STATE)
    purchase_time_millis = models.BigIntegerField(blank=True, null=True)
    status = models.IntegerField(choices=STATUS_TYPES, null=True, blank=True, default=ACTIVE)

    class Meta:
        db_table = "billing_android_receipt"

    def map_receipt(self, receipt):
        if self.kind == self.CONSUMPTION_KIND:
            self.token = receipt['token']
            self.purchase_time_millis = receipt['purchaseTimeMillis']
            self.developer_payload = receipt['developerPayload']
            self.purchase_state = receipt['purchaseState']
            self.consumption_state = receipt['consumptionState']
        elif self.kind == self.SUBSCRIPTION_KIND:
            self.token = receipt['token']
            self.country_code = receipt['countryCode']
            self.developer_payload = receipt['developerPayload']
            self.auto_renewing = receipt['autoRenewing']
            self.price_currency_code = receipt['priceCurrencyCode']
            self.expiry_time_millis = receipt['expiryTimeMillis']
            self.start_time_millis = receipt['startTimeMillis']
            self.price_amount_micros = receipt['priceAmountMicros']


class AndroidConsumableReceipt(AndroidReceipt):

    class Meta:
        proxy = True


class AndroidSubscriptionReceipt(AndroidReceipt):

    class Meta:
        proxy = True

class AppleReceipt(BaseModel):
    ACTIVE = 1
    DEACTIVATE = 0
    STATUS_TYPES = (
        (ACTIVE, 1),
        (DEACTIVATE, 0)
    )

    id = models.CharField(max_length=36, primary_key=True, default=pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    receipt_data = models.TextField(blank=True, null=True)
    cancellation_date = models.DateTimeField(blank=True,null=True)
    quantity = models.IntegerField(blank=True,null=True)
    product_id = models.CharField(max_length=70, blank=True,null=True)
    web_order_line_item_id = models.CharField(max_length=32, blank=True,null=True)
    transaction_id = models.CharField(max_length=32, blank=True,null=True)
    original_transaction_id = models.CharField(max_length=32, blank=True,null=True)
    purchase_date = models.DateTimeField(blank=True,null=True)
    original_purchase_date = models.DateTimeField(blank=True,null=True)
    expires_date = models.DateTimeField(blank=True,null=True)
    status = models.IntegerField(choices=STATUS_TYPES, null=True, blank=True, default=ACTIVE)

    class Meta:
        db_table = "billing_ios_receipt"
        unique_together = ('user','transaction_id')
