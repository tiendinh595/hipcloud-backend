#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from billing.models.common import BaseModel, pkgen

class PurchasePackage(BaseModel):
    """
       All packages for purchasing
    """
    ACTIVE = 1
    DEACTIVATE = 0
    STATUS_TYPES = (
        (ACTIVE, 1),
        (DEACTIVATE, 0)
    )

    DAY_FREQ = "day"
    WEEK_FREQ = "week"
    MONTH_FREQ = "month"
    YEAR_FREQ = "year"
    FREQUENCY_TYPES = (
        (DAY_FREQ, "day"), (WEEK_FREQ, "week"),
        (MONTH_FREQ, "month"), (YEAR_FREQ, "year")
    )

    CONTINUE_SUBSCRIPTION = "continue"
    CANCEL_SUBSCRIPTION = "cancel"
    INITIAL_FAIL_AMOUNT_ACTION = (
        (CONTINUE_SUBSCRIPTION, "continue"),
        (CANCEL_SUBSCRIPTION, "cancel")
    )

    ANDROID_PLATFORM = "android"
    IOS_PLATFORM = "ios"
    WEB_PLATFORM = "web"
    NONE_PLATFORM = "none"
    PLATFORM_CHOICES = (
        (ANDROID_PLATFORM, "android"),
        (IOS_PLATFORM, "ios"),
        (WEB_PLATFORM, "web"),
        (NONE_PLATFORM, "none")
    )

    PAYPAL_GATEWAY = "paypal"
    STRIPE_GATEWAY = "stripe"
    ANDROID_GATEWAY = "android"
    IOS_GATEWAY = "ios"
    CODE_GATEWAY = "code"
    PAYMENT_GATEWAYS = (
        (PAYPAL_GATEWAY, 'paypal'),
        (STRIPE_GATEWAY, 'stripe'),
        (ANDROID_GATEWAY, 'android'),
        (IOS_GATEWAY, 'ios'),
        (CODE_GATEWAY, "code")
    )

    MINIMUM_FUNCTION = 10
    PARTIAL_FUNCTION = 21
    FULL_FUNCTION = 31
    FUNCTIONAL_LEVEL = (
        (MINIMUM_FUNCTION, "Minimum Function"),
        (PARTIAL_FUNCTION, "Partial Function"),
        (FULL_FUNCTION, "Full Function")
    )

    SUBSCRIPTION_TYPE = "sub"
    CONSUMABLE_TYPE = "con"
    CODE_TYPE = "cod"
    PACKAGE_TYPE = (
        (SUBSCRIPTION_TYPE, "sub"),
        (CONSUMABLE_TYPE, "con"),
        (CODE_TYPE, "cod")
    )

    id = models.CharField(max_length=65, primary_key=True, default=pkgen)
    # name of the package ( ex : khuyen_mai_thang_10.. )
    name = models.CharField(max_length=36, null= True, blank=True)
    description = models.TextField(blank=True, null=True)

    type = models.CharField(max_length=5, blank=True, null=True, choices=PACKAGE_TYPE, default=CONSUMABLE_TYPE)
    # Platform
    platform = models.CharField(max_length=7, blank=True, null=True, choices=PLATFORM_CHOICES)
    # Price, Currency, Gateway ( Payment stuffs )
    # Currency USD/VND
    currency = models.CharField(max_length=5, blank=True, null=True, help_text= "USD/VND")
    # Fee to allow first time trial
    setup_fee = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    # Price of package
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    # Subscription cycle
    cycle = models.IntegerField(blank=False, null=False, default=10)
    # Maximum attempt after a charge fail
    max_fail_attempts = models.IntegerField(blank=False, null=False, default=2)
    # What to do if charge fail for setup_fee
    initial_fail_amount_action = models.CharField(choices=INITIAL_FAIL_AMOUNT_ACTION, max_length=20, blank=True,null=True)
    # Gateway of the package
    gateway = models.CharField(choices=PAYMENT_GATEWAYS, max_length=10, blank=True, null=True)
    # Color
    color = models.CharField(max_length=30, blank=True, null=True, help_text = "FROMCOLOR TOCOLOR")
    # Market_id
    market_id = models.CharField(max_length=40, blank=True, null=True)
    # Value of package ( Subscription Values )
    # Go with frequency type ( 1 + Month = 1 month )
    trial_period = models.IntegerField(blank=False, null=False)
    # Types : day, month, week, year
    frequency = models.CharField(choices=FREQUENCY_TYPES, max_length=10, blank=True, null=True)
    # Go with frequency type ( 1 + Month = 1 month ) - Value of package
    frequency_interval = models.IntegerField(blank=False, null=False)
    # Storage capacity
    storage = models.BigIntegerField(blank=True, null=True)
    # Functional capacity
    functional_level = models.IntegerField(blank=False, null=False, default=MINIMUM_FUNCTION)

    # Active / Disable
    status = models.IntegerField(choices=STATUS_TYPES, null=True, blank=True, default=ACTIVE)

    class Meta:
        db_table = "billing_purchase_package"

    def get_charge_date(self):
        frequency = {"day": 1, "week": 7, "month": 30, "year": 365}
        return frequency[self.frequency.lower()] * self.frequency_interval