#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from cas.models.account import Account
from billing.models.common import BaseModel, pkgen
from rest_framework.exceptions import PermissionDenied , ParseError
from billing.models.purchase_package import PurchasePackage
from django.utils import timezone
import logging

logger = logging.getLogger(__name__)

class Partner(BaseModel):
    """
       Storing all the partner of the system ( ex : thegioididong, cmn .. )
    """
    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    name = models.CharField(max_length = 250,blank = True, null = True)
    description = models.TextField(blank = True, null = True)
    type = models.CharField(max_length = 250,blank = True, null = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'billing_partner'

class Bundle(BaseModel):
    """
        Bundle contain multiple code
    """
    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    partner = models.ForeignKey(Partner, blank=True, null=True, db_constraint=False)
    name = models.CharField(max_length = 250,blank = True, null = True)
    description = models.TextField(blank = True, null = True)
    type = models.CharField(max_length = 250,blank = True, null = True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'billing_bundle'


class CodeBank(BaseModel):
    """
       Store all the codes
       Rule of thumb : set storage to the higher , increase subscription time
    """
    SUBSCRIPTION_CODE_TYPE = 'subscription'
    REFERRAL_CODE_TYPE = 'referral'
    DISCOUNT_CODE_TYPE = 'discount'

    CODE_TYPES = (
        (SUBSCRIPTION_CODE_TYPE,'Subscription Code'),
        (REFERRAL_CODE_TYPE,'Referral Code'),
        (DISCOUNT_CODE_TYPE, 'Discount Code')
    )

    DAY = 'day'
    WEEK = 'week'
    MONTH = 'month'
    YEAR = 'year'
    FREQUENCY_TYPES = (
        (DAY,'DAY'), (WEEK,'WEEK'),
        (MONTH,'MONTH'), (YEAR,'YEAR')
    )

    INACTIVE = 0
    ACTIVE = 1
    CANCELED = 2
    USED = 3
    CODE_STATUS_TYPES = (
        (INACTIVE,'InActive'), (ACTIVE,'Active'),
        (CANCELED,'Canceled'), (USED,'Used')
    )

    FIXED_AMOUNT_DISCOUNT = 'fix'
    PERCENTAGE_DISCOUNT = 'percent'
    DISCOUNT_TYPES = (
        (PERCENTAGE_DISCOUNT, 'percent'),
        (FIXED_AMOUNT_DISCOUNT, 'fix')
    )

    UNIQUE_TYPE = 'unique'
    REGULAR_TYPE = 'regular'
    ACTIVE_TYPES = (
        (REGULAR_TYPE,'Regular'), (UNIQUE_TYPE,'Unique')
    )

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    bundle = models.ForeignKey(Bundle, blank=True, null=True, db_constraint=False)
    code = models.CharField(max_length = 25, blank = False, null = False, unique=True)
    code_type = models.CharField(choices=CODE_TYPES, max_length=20, blank=True, null=True)
    max_used = models.IntegerField(default=1, null=True, blank=True)
    discount_type = models.CharField(choices=DISCOUNT_TYPES, max_length=20, blank=True, null=True)
    # Store value of code
    purchase_package = models.ForeignKey(PurchasePackage, blank=True, null=True, db_constraint=False)

    serial = models.CharField(max_length = 125, blank = True, null = True, unique=True)
    active_type = models.CharField(choices = ACTIVE_TYPES, max_length=20, blank=True, null=True)
    condition = models.TextField(blank=True, null=True)
    # Code config
    expiry_date = models.DateTimeField(blank=True, null = True)
    # Code price
    currency = models.CharField(max_length=5, blank=True, null=True, help_text="Code currency usd/vnd/...")
    amount = models.DecimalField(max_digits=10, decimal_places=2, null=True, blank=True)
    status = models.IntegerField(choices = CODE_STATUS_TYPES,null = True, blank = True, default = 1)

    class Meta:
        db_table = 'billing_code_bank'

    def __init__(self, *args, **kwargs):
        super(CodeBank, self).__init__(*args, **kwargs)
        if self.code_type == CodeBank.REFERRAL_CODE_TYPE:
            self.__class__ = ReferralCode
        elif self.code_type == CodeBank.SUBSCRIPTION_CODE_TYPE:
            self.__class__ = SubscriptionCode
        elif self.code_type == CodeBank.DISCOUNT_CODE_TYPE:
            self.__class__ = DiscountCode

    def activable(self):
        if self.status != self.ACTIVE:
            return False

        if self.expiry_date:
            current_time = timezone.now()
            if self.expiry_date < current_time:
                return False
        return True

    # def active_code(self, user_id, platform = None, ip = None, captcha = None):
    #     if self.status != self.ACTIVE:
    #         return False
    #
    #     if self.expiry_date:
    #         current_time = timezone.now()
    #         if self.expiry_date < current_time:
    #             return False
    #
    #     if self.active_type == self.UNIQUE_TYPE:
    #         activator =  UserActiveUniqueCode(user_id=user_id, code=self)
    #     else:
    #         activator = UserActiveRegularCode(user_id=user_id, code=self)
    #
    #     activator.captcha = captcha
    #     activator.platform = platform
    #     activator.user_ip = ip
    #     activator.save()
    #     self.status = self.USED
    #     return self, activator
    #
    # def get_activator(self, user_id):
    #     if self.active_type == self.UNIQUE_TYPE:
    #         return UserActiveUniqueCode(user_id=user_id, code=self)
    #     else:
    #         return UserActiveRegularCode(user_id=user_id, code=self)

    def __str__(self):
        return "%s | %s | %s" % (self.code , self.code_type , self.status)


class ReferralCode(CodeBank):
    """
       Store all the referral code
       Referral code : active multiple time , increase storage , have limit quota cap
    """

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.code_type = DiscountCode.REFERRAL_CODE_TYPE
        super(ReferralCode, self).save(*args, **kwargs)

    def active_code(self, user_id, platform = None, ip = None, captcha = None):
        if self.status != self.ACTIVE:
            return False

        if self.expiry_date:
            current_time = timezone.now()
            if self.expiry_date < current_time:
                return False

        if self.active_type == self.UNIQUE_TYPE:
            activator =  UserActiveUniqueCode(user_id=user_id, code=self)
            self.status = self.USED
        else:
            activator = UserActiveRegularCode(user_id=user_id, code=self)

        activator.captcha = captcha
        activator.platform = platform
        activator.user_ip = ip
        return activator

class SubscriptionCode(CodeBank):
    """
       Store all the subscription code
       Referral code : active one time per user, set storage size fixed,
    """

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.code_type = DiscountCode.SUBSCRIPTION_CODE_TYPE
        super(SubscriptionCode, self).save(*args, **kwargs)

    def active_code(self, user_id, platform = None, ip = None, captcha = None):
        if self.status != self.ACTIVE:
            raise ParseError("Code activated")

        if self.expiry_date:
            current_time = timezone.now()
            if self.expiry_date < current_time:
                raise ParseError("Code expired")

        if self.active_type == self.UNIQUE_TYPE:
            activator =  UserActiveUniqueCode(user_id=user_id, code=self)
            self.status = self.USED
        else:
            activator = UserActiveRegularCode(user_id=user_id, code=self)

        try:
            activator.captcha = captcha
            activator.platform = platform
            activator.user_ip = ip
        except Exception as ex:
            raise ParseError("Code activation fail")
        return activator

class DiscountCode(CodeBank):
    """
       Store all the discount code
       Referral code : active one time per user, decree price for transaction about to happen
    """

    class Meta:
        proxy = True

    def save(self, *args, **kwargs):
        self.code_type = DiscountCode.DISCOUNT_CODE_TYPE
        super(DiscountCode, self).save(*args, **kwargs)

    def active_code(self, user_id, platform = None, ip = None, captcha = None):
        if self.status != self.ACTIVE:
            return False

        if self.expiry_date:
            current_time = timezone.now()
            if self.expiry_date < current_time:
                return False

        if self.active_type == self.UNIQUE_TYPE:
            activator =  UserActiveUniqueCode(user_id=user_id, code=self)
            self.status = self.USED
        else:
            max_turns_allow_to_use_code = getattr(self.code, 'max_used', None)
            if max_turns_allow_to_use_code:
                results = UserActiveRegularCode.objects.filter(code=self.code).aggregate(models.Count('id'))
                if results and 'id__count' in results and results['id__count'] > max_turns_allow_to_use_code:
                    raise ParseError("Số lượng lượt sử dụng code đã đạt tối đa.")
            activator = UserActiveRegularCode(user_id=user_id, code=self)

        activator.captcha = captcha
        activator.platform = platform
        activator.user_ip = ip
        return activator

class UserActiveRegularCode(BaseModel):

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    code = models.ForeignKey(CodeBank, blank=True, null=True, db_constraint=False, db_index=True)
    captcha = models.TextField(blank=True, null=True)
    platform = models.CharField(max_length=10, blank=True, null=True)
    user_ip = models.CharField(max_length=40, blank=True, null=True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'billing_user_active_referral_code'
        unique_together = ('user','code')

class UserActiveUniqueCode(BaseModel):

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    code = models.OneToOneField(CodeBank, blank=True, null=True, unique=True, db_constraint=False)
    captcha = models.TextField(blank=True, null=True)
    platform = models.CharField(max_length=10, blank=True, null=True)
    user_ip = models.CharField(max_length=40, blank=True, null=True)
    status = models.IntegerField(null = True, blank = True, default = 1)

    class Meta:
        db_table = 'billing_user_active_purchase_code'