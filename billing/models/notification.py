#!/usr/bin/env python
# -*- coding: utf-8 -*-
from billing.models.common import BaseModel, pkgen
from cas.models.account import Account
from django.db import models
import logging


logger = logging.getLogger(__name__)


class NotificationMessage(BaseModel):
    """
       Storing message
    """
    UNACTIVE = 0
    ACTIVE = 1



    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    type = models.IntegerField( blank=True, null=True)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    message = models.TextField(blank = True, null = True)
    status = models.IntegerField(null = True, blank = True, default = 1)


    class Meta:
        db_table = 'billing_notification_message'