#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db.models.base import ModelBase
from django.db import models
import uuid

def pkgen():
    return str(uuid.uuid4().hex)

def readable_id():
    return str(uuid.uuid4().hex)[:8]

class BaseModel(models.Model):

    created_at = models.DateTimeField(blank=True, null = True, auto_now_add = True)
    updated_at = models.DateTimeField(blank=True, null = True, auto_now = True)

    class Meta:
        abstract = True
