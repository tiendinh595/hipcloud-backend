#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.db import models
from cas.models.account import Account
from billing.models.common import BaseModel, pkgen, readable_id
from billing.models.purchase_package import PurchasePackage
from billing.models.code_bank import CodeBank
from django.utils import timezone

class UserTransaction(BaseModel):

    AUTHORIZED = 0
    AUTHORIZATION_EXPIRED = 1
    PROCESSOR_DECLINED = 2
    GATEWAY_REJECTED = 3
    FAILED = 4
    VOIDED = 5
    SUBMITTED = 6
    SETTLING = 7
    SETTLED = 8
    SETTLEMENT_DECLINED = 9
    SETTLEMENT_PENDING = 10
    REFUND = 11
    REFUNDED = 12

    TRANSACTION_STATES = (
        (AUTHORIZED, 'Authorized'),

        (AUTHORIZATION_EXPIRED, 'Authorization expired'),
        (PROCESSOR_DECLINED, 'Processor declined'),      # The processor declined the transaction. The processor response code has information about why the transaction was declined.
        (GATEWAY_REJECTED, 'Gateway rejected'),        # The gateway rejected the transaction because AVS, CVV, duplicate, or fraud checks failed

        (FAILED, 'Failed'),                  # An error occurred when sending the transaction to the processor.
        (VOIDED, 'Voided'),                  # The transaction was voided. You can void transactions when the status is (0, 'Authorized') or (6, 'Submitted for settlement').
                                             # After the transaction has been settled, you will have to refund the transaction instead.
        (SUBMITTED, 'Submitted for settlement'),
        (SETTLING, 'Settling'),                             # The transaction is in the process of being settled. This is a transitory state.
        (SETTLED , 'Settled'),                              # Success
        (SETTLEMENT_DECLINED , 'Settlement declined'),      # The processor declined to settle the sale or refund request, and the result is unsuccessful.
        (SETTLEMENT_PENDING , 'Settlement pending'),        # The transaction has not yet settled successfully, but we expect it to settle soon.
                                                            # This status is very rare and will only appear for PayPal transactions.
        (REFUND, 'Refund'),
        (REFUNDED, 'Refunded')
    )

    CODE_TYPE = "CODE"
    CREATE_SUBSCRIPTION_TYPE = "CREATE"
    TRIAL_SUBSCRIPTION_TYPE = "TRIAL"
    SUBSCRIBE_TYPE = "SUBSCRIBE"
    CHANGE_PLAN_TYPE = "CHANGE_PLAN"
    RENEW_TYPE = "RENEW"
    CANCEL_TYPE = "CANCEL"
    SUSPEND_TYPE = "SUSPEND"
    REACTIVE_TYPE = "REACTIVE"
    TRANSACTION_TYPE = (
        (CODE_TYPE, 'CODE SUBSCRIPTION'),
        (CREATE_SUBSCRIPTION_TYPE, 'CREATE SUBSCRIPTION'),
        (TRIAL_SUBSCRIPTION_TYPE, 'TRIAL SUBSCRIPTION'),
        (SUBSCRIBE_TYPE, 'ACTIVE SUBSCRIPTION'),
        (CHANGE_PLAN_TYPE, 'UPDATE (PACKAGE) SUBSCRIPTION PLAN'),
        (RENEW_TYPE, 'RENEW SUBSCRIPTION'),
        (CANCEL_TYPE, 'CANCEL SUBSCRIPTION'),
        (SUSPEND_TYPE, 'SUSPEND SUBSCRIPTION'),
        (REACTIVE_TYPE, 'REACTIVE SUBSCRIPTION')
    )

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    type = models.CharField(choices=TRANSACTION_TYPE, max_length=15, blank=True, null=True, default=SUBSCRIBE_TYPE,
                            help_text="CODE/SUBSCRIBE/CANCEL/CHANGE_PLAN")
    description = models.TextField(null=True, blank=True)
    purchase_package = models.ForeignKey(PurchasePackage, blank=True, null=True, db_constraint=False, help_text="Id of package purchased")
    active_code = models.ForeignKey(CodeBank, blank=True, null=True, db_constraint=False)

    platform = models.CharField(max_length=10, blank=True, null=True, help_text='ios/android/web')
    currency = models.CharField(max_length=10, blank=True, null=True, help_text='usd/vnd')
    amount = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, help_text='ex : 10.03')
    setup_fee = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True)

    readable_id = models.CharField(max_length=10, blank=True, null=True, db_index=True, default = readable_id)
    status = models.IntegerField(choices=TRANSACTION_STATES,blank=True, null=True, default=0)

    class Meta:
        db_table = "billing_transaction"

class Subscription(BaseModel):

    # May be first time purchase fail
    ACTIVE = 1
    INACTIVE = 0
    # User cancel or not paid, resumable
    CANCEL = 2
    # Totally non-resumable
    SUSPENDED = 3

    STATUS_TYPES = (
        (INACTIVE, 'Inactive'),
        (ACTIVE, 'Active'),
        (CANCEL, 'Cancel'),
        (SUSPENDED, 'Suspended'),
    )

    MINIMUM_FUNCTION = 10
    PARTIAL_FUNCTION = 21
    FULL_FUNCTION = 31
    FUNCTIONAL_LEVEL = (
        (MINIMUM_FUNCTION, "Minimum Function"),
        (PARTIAL_FUNCTION, "Partial Function"),
        (FULL_FUNCTION, "Full Function")
    )


    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    user = models.OneToOneField(Account, blank=True, null=True, db_constraint=False)
    suspended_date = models.DateTimeField(blank=True, null=True)
    expiry_date = models.DateTimeField(blank=True, null=True)
    storage = models.BigIntegerField(blank=True, null=True)
    # This package is reference tp minimum package of user
    package = models.ForeignKey(PurchasePackage, blank=True, null=True, db_constraint=False)
    functional_level = models.IntegerField(blank=False, null=True, default=MINIMUM_FUNCTION)
    status = models.IntegerField(null = True, blank = True, default = ACTIVE)

    class Meta:
        db_table = 'billing_subscription'

    def is_active(self):
        """
        Check subscription still valid
        :return: boolean
        """
        return (self.status == self.ACTIVE) and (self.expiry_date > timezone.now())

    def available_for_purchase(self, package_to_buy):
        """
        Check user available for a upgrade or new purchase
        :param package_to_buy: Package Object
        :return: boolean
        """
        if self.storage:
            return (package_to_buy.storage < self.storage and self.status == self.ACTIVE)
        else:
            return True


class SubscriptionDetail(BaseModel):

    # May be first time purchase fail
    ACTIVE = 1
    INACTIVE = 0
    # User cancel or not paid, resumable
    CANCEL = 2
    # Totally non-resumable
    SUSPENDED = 3
    EXPIRED = 4

    STATUS_TYPES = (
        (INACTIVE, 'Inactive'),
        (ACTIVE, 'Active'),
        (CANCEL, 'Cancel'),
        (SUSPENDED, 'Suspended'),
        (EXPIRED, 'Expired'),
    )

    id = models.CharField(max_length = 36, primary_key = True, default = pkgen)
    user = models.ForeignKey(Account, blank=True, null=True, db_constraint=False)
    suspended_date = models.DateTimeField(blank=True, null=True)
    expiry_date = models.DateTimeField(blank=True, null=True)
    storage = models.BigIntegerField(blank=True, null=True)
    package = models.ForeignKey(PurchasePackage, blank=True, null=True, default=None, db_constraint=False)
    status = models.IntegerField(null = True, blank = True, default = ACTIVE)

    class Meta:
        db_table = 'billing_subscription_detail'
        index_together = [
            ['user', 'status', 'expiry_date'],             # for sum storage by user
            ['status', 'expiry_date']                      # for periodic job subscription checker
        ]


class FunctionalLevel(BaseModel):

    ACTIVE = 1
    INACTIVE = 0

    id = models.CharField(max_length=36, primary_key=True, default=pkgen)
    name = models.CharField(max_length=36, null=True, blank=True)
    level = models.IntegerField(null=True, blank=True, default=0)
    status = models.IntegerField(null=True, blank=True, default=ACTIVE)

    class Meta:
        db_table = 'billing_functional_level'
