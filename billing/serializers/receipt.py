#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from billing.models.receipt import AppleReceipt, AndroidReceipt



class AndroidReceiptSerializer(serializers.ModelSerializer):


    class Meta:
        model = AndroidReceipt
        fields = ("kind", "start_time_millis", "expiry_time_millis", "auto_renewing", "price_currency_code",
                  "price_amount_micros", "country_code", "developer_payload", "payment_state", "cancel_reason")


class AppleReceiptSerializer(serializers.ModelSerializer):


    class Meta:
        model = AppleReceipt
        fields = ("user", "quantity", "product_id" , "transaction_id", "original_transaction_id",
                  "purchase_date", "original_purchase_date")
