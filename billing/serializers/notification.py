#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from billing.models.notification import NotificationMessage



class NotificationSerializer(serializers.ModelSerializer):


    class Meta:
        model = NotificationMessage
        fields = ('type','user','message','created_at')
