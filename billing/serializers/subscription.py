#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from billing.models.subscription import Subscription, UserTransaction
import json



class InitTransactionSerializer(serializers.ModelSerializer):

    class Meta:
        model = UserTransaction
        fields = ("id", "user", "type", "purchase_package", "readable_id")


class PrivateSubscriptionSerializer(serializers.ModelSerializer):

    class Meta:
        model = Subscription


class SubscriptionSerializer(serializers.ModelSerializer):

    package = serializers.SerializerMethodField(source='get_package')

    class Meta:
        model = Subscription
        fields = ( "user", "expiry_date", "storage", "package")

    def get_package(self, subscription):
        user_package = subscription.package
        try:
            if user_package:
                return user_package.name
            else:
                return "VIP package"
        except:
            return "VIP package."
