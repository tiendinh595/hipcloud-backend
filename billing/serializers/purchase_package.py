#!/usr/bin/env python
# -*- coding: utf-8 -*-

from rest_framework import serializers
from billing.models.purchase_package import PurchasePackage
import json

class WebPurchasePackageSerializer(serializers.ModelSerializer):

    color = serializers.SerializerMethodField(source='get_color', help_text = "['#FROMCOLOR', '#TOCOLOR']")
    is_current = serializers.CharField(source='get_is_current', help_text = "true/false")

    class Meta:
        model = PurchasePackage
        fields = ("id", "name","description","currency", "amount", "color", "frequency", "trial_period",
                  "frequency_interval", "storage", "is_current")


    def get_color(self, package):
        return json.loads(package.color)


    def get_is_current(self, package):
        storage_limit = self.context.get("storage_limit")
        if storage_limit:
            if package.storage == storage_limit:
                return True
        return False


class IOSPurchasePackageSerializer(serializers.ModelSerializer):

    color = serializers.SerializerMethodField(source='get_color', help_text = "['#FROMCOLOR', '#TOCOLOR']")
    is_current = serializers.SerializerMethodField(source='get_is_current', help_text = "true/false")

    class Meta:
        model = PurchasePackage
        fields = ("id", "name", "type","description","currency", "amount", "color", "frequency", "trial_period",
                  "frequency_interval", "storage", "market_id", "is_current")


    def get_color(self, package):
        return json.loads(package.color)


    def get_is_current(self, package):
        storage_limit = self.context.get("storage_limit")
        if storage_limit:
            if package.storage == storage_limit:
                return True
        return False


class AndroidPurchasePackageSerializer(serializers.ModelSerializer):

    color = serializers.SerializerMethodField(source='get_color', help_text = "['#FROMCOLOR', '#TOCOLOR']")
    is_current = serializers.SerializerMethodField(source='get_is_current', help_text = "true/false")

    class Meta:
        model = PurchasePackage
        fields = ("id", "name", "type","description","currency", "amount", "color", "frequency", "trial_period",
                  "frequency_interval", "storage", "market_id", "is_current")

    def get_color(self, package):
        return json.loads(package.color)


    def get_is_current(self, package):
        storage_limit = self.context.get("storage_limit")
        if storage_limit:
            if package.storage == storage_limit:
                return True
        return False

