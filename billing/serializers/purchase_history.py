#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from billing.models.subscription import UserTransaction


class PurchaseHistorySerializer(serializers.ModelSerializer):


    message = serializers.SerializerMethodField(source='get_message')


    class Meta:
        model = UserTransaction
        fields = ('type','user','message','purchase_package','active_code','platform','readable_id','created_at')


    def get_message(self, transaction):
        if transaction.active_code:
            print transaction.__dict__
            return "Bạn đã nhập mả " + transaction.active_code_id
        else:
            return ""