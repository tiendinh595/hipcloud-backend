#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework import serializers
from billing.models.code_bank import CodeBank


class CodeBankSerializer(serializers.ModelSerializer):


    class Meta:
        model = CodeBank
        fields = ("code", "code_type")