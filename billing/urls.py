#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.conf.urls import include, url
from billing.views import active_code
import billing.views.purchase_package as packages
import billing.views.in_app_purchase as iap
from billing.views import subscription
from billing.views import notification
from billing.views import purchase_history
from django.contrib import admin

urlpatterns = [

    url(r'^payment/history', purchase_history.PurchaseHistoryView.as_view()),
    url(r'^notification/', notification.NotificationMessageView.as_view()),
    url(r'^private/subscription', subscription.UserSubscriptionBackend.as_view()),
    url(r'^subscription/info', subscription.UserSubscription.as_view()),
    url(r'^packages/ios/', packages.PurchasePackageIos.as_view()),
    url(r'^packages/android/', packages.PurchasePackageAndroid.as_view()),
    url(r'^packages/web/', packages.PurchasePackageWeb.as_view()),
    url(r'^code/active', active_code.ActiveCode.as_view()),

    url(r'^init_transaction/', iap.InitTransaction.as_view()),
    url(r'^verify_receipt/ios/', iap.ApplePurchaseValidate.as_view()),
    url(r'^verify_receipt/android/', iap.AndroidPurchaseValidate.as_view()),
    url(r'^docs/', include('rest_framework_swagger.urls'))
]