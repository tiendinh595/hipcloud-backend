#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.exceptions import APIException


class InternalError(APIException):

    status_code = 500
    default_detail = 'System Error. Please try again later'
    default_code = 'service_unavailable'


class OutOfQuota(APIException):

    status_code = 413
    default_detail = 'Quota limit is reached'
    default_code = 'service_unavailable'