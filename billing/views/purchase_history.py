#!/usr/bin/env python
# -*- coding: utf-8 -*-
from billing.serializers.purchase_history import PurchaseHistorySerializer
from billing.models.subscription import UserTransaction
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated
from rest_framework.views import APIView
from api.service import response
from django.conf import settings
from django.utils import timezone
import logging


logger = logging.getLogger(__name__)


class PurchaseHistoryView(APIView):


    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)


    def post(self, request, *args, **kwargs):
        """
        List all history transaction
        ---

        serializer: billing.serializers.purchase_history.PurchaseHistorySerializer

        parameters_strategy: replace

        many: true

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth

        purchase_history = UserTransaction.objects.filter(user_id=user_id,
                                                                  status=UserTransaction.SETTLED).order_by('-created_at')

        serialized = PurchaseHistorySerializer(purchase_history, many=True)
        return response.success(serialized.data)