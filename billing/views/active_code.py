#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.db.utils import IntegrityError
from api.authentication import TokenAuthentication
from rest_framework.exceptions import ParseError
from api.permissions import TokenIsAuthenticated
from billing.models.code_bank import CodeBank
from rest_framework.views import APIView
from billing.services.block import sync_container_info
from billing.services.subscription import add_user_subscription
from api.service import response
from django.db import transaction
import logging


logger = logging.getLogger(__name__)


class ActiveCode(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)


    def post(self, request, *args, **kwargs):
        """
        User consume referral code / gift code/ discount code
        ---

        type :
            message:
                required: true
                type: string
                description: Message to display to user

        parameters_strategy: replace

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: code
              description: user's code
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        param_data = request.POST
        code = param_data.get('code', None)
        captcha = param_data.get('captcha', None)
        captcha_key = param_data.get('captcha_key', None)

        if not code:
            raise ParseError("Code not found")

        try:
            # Active code
            user_code = CodeBank.objects.get(code = code, status = CodeBank.ACTIVE)
            activator = user_code.active_code(user_id, platform = None, ip = None, captcha = captcha)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Code not found.")

        # Create subscription
        user_package = user_code.purchase_package
        user_subscription, user_subscription_detail = add_user_subscription(user_id, user_package)

        # Save code, activator, subscription in one transaction
        try:
            with transaction.atomic():
                user_code.save()
                activator.save()
                user_subscription.save()
                user_subscription_detail.save()
                sync_response = sync_container_info(user_id, user_subscription.storage)
        except IntegrityError as ex:
            logger.warning(ex)
            raise ParseError("Bạn đã sử dụng code rồi vui lòng không nhập lại.")
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Internal Error . Please try again")
        return response.success({"message":"Bạn đã nạp thành công code"})
