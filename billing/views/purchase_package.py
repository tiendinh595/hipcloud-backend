#!/usr/bin/env python
# -*- coding: utf-8 -*-
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated
from rest_framework.views import APIView
from api.service import response
from billing.models.purchase_package import PurchasePackage
from billing.serializers.purchase_package import WebPurchasePackageSerializer, IOSPurchasePackageSerializer,\
    AndroidPurchasePackageSerializer
from billing.models.subscription import Subscription
from django.conf import settings
import logging
from django.utils import timezone

logger = logging.getLogger(__name__)

class PurchasePackageAndroid(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        List all package for android
        ---

        many: true

        serializer: billing.serializers.purchase_package.AndroidPurchasePackageSerializer

        parameters_strategy: replace

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        packages = PurchasePackage.objects.filter(status=PurchasePackage.ACTIVE,
                                                  platform=PurchasePackage.ANDROID_PLATFORM).order_by('storage')
        # Get current user subscription
        storage_limit = None
        try:
            subscription = Subscription.objects.get(user_id=user_id,status=Subscription.ACTIVE,
                                                    expiry_date__gte=timezone.now())
            storage_limit = subscription.storage
        except:
            pass
        serialized = AndroidPurchasePackageSerializer(packages, many=True, context={'user_id': user_id, 'storage_limit': storage_limit})
        return response.success(serialized.data)


class PurchasePackageIos(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        List all package for ios
        ---
        many: true

        serializer: billing.serializers.purchase_package.IOSPurchasePackageSerializer

        parameters_strategy: replace

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        packages = PurchasePackage.objects.filter(status=PurchasePackage.ACTIVE,
                                                  platform=PurchasePackage.IOS_PLATFORM).order_by('storage')
        # Get current user subscription
        storage_limit = None
        try:
            subscription = Subscription.objects.get(user_id=user_id,status=Subscription.ACTIVE,
                                                    expiry_date__gte=timezone.now())
            storage_limit = subscription.storage
        except:
            pass
        serialized = IOSPurchasePackageSerializer(packages, many=True, context={'user_id': user_id, 'storage_limit': storage_limit})
        return response.success(serialized.data)


class PurchasePackageWeb(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        List all package for web
        ---

        many: true

        serializer: billing.serializers.purchase_package.WebPurchasePackageSerializer

        parameters_strategy: replace

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug `token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        packages = PurchasePackage.objects.filter(status=PurchasePackage.ACTIVE,
                                                  platform=PurchasePackage.WEB_PLATFORM).order_by('storage')
        # Get current user subscription
        storage_limit = None
        try:
            subscription = Subscription.objects.get(user_id=user_id,status=Subscription.ACTIVE,
                                                    expiry_date__gte=timezone.now())
            storage_limit = subscription.storage
        except:
            pass
        serialized = WebPurchasePackageSerializer(packages, many=True, context={'user_id': user_id, 'storage_limit': storage_limit})
        return response.success(serialized.data)