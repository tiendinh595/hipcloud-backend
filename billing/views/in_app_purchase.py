#!/usr/bin/env python
# -*- coding: utf-8 -*-
from api.authentication import TokenAuthentication
from api.permissions import TokenIsAuthenticated
from rest_framework.views import APIView
from api.service import response
from django.utils import timezone
from django.db import transaction
from datetime import timedelta
from rest_framework.exceptions import ParseError
from billing.services.block import sync_container_info
from billing.services.subscription import add_user_subscription
from billing.models.receipt import AppleReceipt, AndroidReceipt, AndroidSubscriptionReceipt, AndroidConsumableReceipt
from billing.models.purchase_package import PurchasePackage
from billing.models.subscription import UserTransaction, Subscription
from billing.serializers.subscription import InitTransactionSerializer
from billing.serializers.purchase_package import WebPurchasePackageSerializer, IOSPurchasePackageSerializer,\
    AndroidPurchasePackageSerializer
from billing.serializers.receipt import AppleReceiptSerializer
from billing.services.iap.app_store import AppStoreValidator
from billing.services.iap.app_store import InAppValidationError
from billing.models.subscription import Subscription
from oauth2client.service_account import ServiceAccountCredentials
from apiclient import discovery
from datetime import datetime
from django.conf import settings
import logging
import pytz
utc = pytz.utc


logger = logging.getLogger(__name__)



class InitTransaction(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Init transaction for processing
        ---

        serializer: billing.serializers.subscription.InitTransactionSerializer

        parameters_strategy: replace

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: package_id
              description: package id from system response
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        param_data = request.POST
        package_id = param_data.get('package_id', None)
        # Check token platform

        # check package exists
        try:
            package_to_buy = PurchasePackage.objects.get(id=package_id)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Package not found")
        # Check subscription is upgrade package
        user_subscription = None
        try:
            user_subscription = Subscription.objects.get(user_id = user_id,
                                                         status = Subscription.ACTIVE,
                                                         expiry_date__gte = timezone.now())
        except Exception as ex:
            pass

        if user_subscription:
            if package_to_buy.storage < user_subscription.storage:
                raise ParseError("Subscription already exists")

        # Create transaction
        transaction = UserTransaction(
            user_id = user_id,
            type = UserTransaction.SUBSCRIBE_TYPE,
            purchase_package = package_to_buy,
            platform = package_to_buy.platform,
            currency = package_to_buy.currency,
            amount = package_to_buy.amount,
            status = UserTransaction.AUTHORIZED
        )
        transaction.save()

        serialized = InitTransactionSerializer(transaction)
        return response.success(serialized.data)


class AndroidPurchaseValidate(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Validate purchase for android / active subscription accordingly
        ---
        type:

          status:
            required: true
            type: string

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: transaction_id
              description: id of the transaction
              required: false
              type: string
              paramType: form

            - name: product_id
              description: id of the product
              required: false
              type: string
              paramType: form

            - name: android_token
              description: purchase token after android iap
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"status": "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        param_data = request.POST
        upbox_transaction_id = param_data.get('transaction_id', None)
        android_token = param_data.get('android_token', None)
        android_product = param_data.get('product_id', None)

        # Transaction id
        try:
            user_transaction = UserTransaction.objects.get(id = upbox_transaction_id)
            user_package = user_transaction.purchase_package
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Transaction id not found")
        # Find previous subscription
        subscription = None
        try:
            subscription = Subscription.objects.get(user_id=user_id)
        except:
            pass
        # Check receipt in current system
        android_receipt = None
        try:
            android_receipt = AndroidReceipt.objects.get(token=android_token)
        except:
            pass

        # Validate receipt with android
        response_payload = None
        try:
            if user_package.type == PurchasePackage.CONSUMABLE_TYPE:
                response_payload = settings.ANDROID_SERVICE.purchases().products().get(packageName=settings.ANDROID_PACKAGE_ID,
                                                                                       productId=android_product,
                                                                                       token=android_token).execute()
                response_payload['token'] = android_token
            elif user_package.type == PurchasePackage.SUBSCRIPTION_TYPE:
                response_payload = settings.ANDROID_SERVICE.purchases().subscriptions().get(packageName=settings.ANDROID_PACKAGE_ID,
                                                                                            subscriptionId=android_product,
                                                                                            token=android_token).execute()
                response_payload['token'] = android_token
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Google say no")

        try:
            with transaction.atomic():
                # Update receipt
                if android_receipt:
                    android_receipt.map_receipt(response_payload)
                else:
                    android_receipt = AndroidReceipt(
                        user_id=user_id,kind=response_payload['kind'],
                        status=AndroidReceipt.ACTIVE
                    )
                    android_receipt.map_receipt(response_payload)
                android_receipt.save()
                # Create subscription
                subscription, user_subscription_detail = add_user_subscription(user_id, user_package)
                # Update transaction
                user_transaction.status = UserTransaction.SETTLED
                user_transaction.save()
                user_subscription_detail.save()
                subscription.save()
                android_receipt.save()
                # Sync container info with block service
                sync_response = sync_container_info(user_id, subscription.storage)
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Internal Error . Please try again")

        return response.success({"status": "success"})


class ApplePurchaseValidate(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)


    def post(self, request, *args, **kwargs):
        """
        Validate purchase for ios / active subscription accordingly
        ---
        type:

          status:
            required: true
            type: string
            description: "success"

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

            - name: transaction_id
              description: transaction id from upbox service
              required: false
              type: string
              paramType: form

            - name: apple_receipt
              description: apple receipt , long string base64 from apple
              required: false
              type: string
              paramType: form

            - name: is_sandbox
              description: attach data to this param to active sandbox
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: '{"status": "success"}'
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        param_data = request.POST
        upbox_transaction_id = param_data.get('transaction_id', None)
        apple_receipt = param_data.get('apple_receipt', None)
        is_sandbox = param_data.get('is_sandbox', None)

        logger.error("Receive receipt : ")
        logger.error(apple_receipt)
        logger.error("Is Sandbox : ")
        logger.error(is_sandbox)

        if not upbox_transaction_id:
            raise ParseError("Transaction not found")

        if not apple_receipt:
            raise ParseError("Receipt not found")

        try:
            user_transaction = UserTransaction.objects.get(id = upbox_transaction_id)

            user_package = user_transaction.purchase_package
        except Exception as ex:
            logger.error(ex)
            raise ParseError("Transaction id not found")
        if user_transaction.status == UserTransaction.SETTLED:
            raise ParseError("Transaction not found.")

        if is_sandbox:
            validator = AppStoreValidator( settings.APPLE_BUNDLE_ID , sandbox=True )
        else:
            validator = AppStoreValidator( settings.APPLE_BUNDLE_ID , sandbox=False )
        # Find previous subscription
        subscription = None
        try:
            subscription = Subscription.objects.get(user_id=user_id)
        except:
            pass
        # Validate receipt with apple
        try:
            if user_package.type == "sub":
                receipt = validator.validate(apple_receipt, settings.SHARED_APPLE_KEY)
            else:
                receipt = validator.validate(apple_receipt)
            in_app_receipt = receipt['in_app']
        except InAppValidationError as ex:
            logger.error(ex)
            raise ParseError("Receipt is not valid")
        # Validate receipt with internal system
        receipt_list = []
        receipt_transaction_ids = [rec['transaction_id'] for rec in in_app_receipt]
        current_apple_receipt_ids = [rec.transaction_id for rec in AppleReceipt.objects.filter(user_id=user_id,
                                    transaction_id__in=receipt_transaction_ids,
                                    status=AppleReceipt.ACTIVE)]

        for rec in in_app_receipt:
            purchase_date = datetime.strptime(' '.join(rec['purchase_date'].split(" ")[:-1]), '%Y-%m-%d %H:%M:%S').replace(tzinfo=utc)
            original_purchase_date =  datetime.strptime(' '.join(rec['original_purchase_date'].split(" ")[:-1]), '%Y-%m-%d %H:%M:%S').replace(tzinfo=utc)

            if rec['transaction_id'] not in current_apple_receipt_ids:
                receipt_list.append(
                    AppleReceipt(
                        user_id=user_id,
                        receipt_data=apple_receipt,
                        quantity=rec['quantity'],
                        product_id=rec['product_id'],
                        transaction_id=rec['transaction_id'],
                        original_transaction_id=rec['original_transaction_id'],
                        purchase_date=purchase_date,
                        original_purchase_date=original_purchase_date,
                        status=AppleReceipt.ACTIVE
                    )
                )


        if len(receipt_list) > 0:
            try:
                with transaction.atomic():
                    # Update receipt
                    apple_receipt = AppleReceipt.objects.bulk_create(receipt_list)
                    # Create subscription
                    subscription, user_subscription_detail = add_user_subscription(user_id, user_package)
                    # Update transaction
                    user_transaction.status = UserTransaction.SETTLED
                    user_transaction.save()
                    user_subscription_detail.save()
                    subscription.save()
                    # Sync container info with block service
                    sync_response = sync_container_info(user_id, subscription.storage)
            except Exception as ex:
                logger.error(ex)
                raise ParseError("Internal Error . Please try again")
        else:
            logger.error("Not a new transaction was found")
            raise ParseError("Not a new transaction was found")
        #
        # serialized = AppleReceiptSerializer(apple_receipt, many=True)
        # return response.success(serialized.data)
        return response.success({"status":"success"})