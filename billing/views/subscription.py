#!/usr/bin/env python
# -*- coding: utf-8 -*-
from api.authentication import TokenAuthentication, BackendTokenAuthenticate
from api.permissions import TokenIsAuthenticated
from rest_framework.views import APIView
from api.service import response
from billing.serializers.subscription import SubscriptionSerializer, PrivateSubscriptionSerializer
from billing.models.subscription import Subscription
from django.utils import timezone
from django.conf import settings
import logging


logger = logging.getLogger(__name__)

class UserSubscriptionBackend(APIView):

    authentication_classes = (BackendTokenAuthenticate,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        [BACKEND ONLY] Get user subscription status
        ---

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        try:
            subscription = Subscription.objects.get(user_id = user_id, expiry_date__gte=timezone.now())
            serialized = PrivateSubscriptionSerializer(subscription)
            return response.success(serialized.data)
        except Exception as ex:
            logger.info(ex)
            return response.success({"error": 1 , "status":"Not subscribed"})

class UserSubscription(APIView):

    authentication_classes = (TokenAuthentication,)
    permission_classes = (TokenIsAuthenticated,)

    def post(self, request, *args, **kwargs):
        """
        Get user subscription status
        ---

        parameters:
            - name: Accept-Language
              description: vi / en
              required: false
              type: string
              paramType: header

            - name: Authorization
              description: user access token  ( debug token 5075284997574d7f84dd8334a7c1d284 )
              required: true
              type: string
              paramType: header

            - name: is_xhr
              description: flag indicate JavaScript XMLHttpRequest
              required: false
              type: string
              paramType: form

        responseMessages:
            - code: 200
              message: Success Json Object
            - code: 400
              message: '{"error" : 400 , "message" : "Not Valid", "data" : {}}'
            - code: 403
              message: '{"error" : 403 , "message" : "Insufficient rights to call this procedure", "data" : {}}'
            - code: 404
              message: '{"error" : 404 , "message" : "Not Found", "data" : {}}'
            - code: 500
              message: '{"error" : 500 , "message" : "Internal Error", "data" : {}}'

        consumes:
            - application/json

        produces:
            - application/json
        """
        user_id = request.auth
        try:
            subscription = Subscription.objects.get(user_id = user_id, expiry_date__gte=timezone.now())
            serialized = SubscriptionSerializer(subscription)
            return response.success(serialized.data)
        except Exception as ex:
            logger.info(ex)
            return response.success({"error": 1 , "status":"Not subscribed"})

