import logging
from datetime import timedelta
from django.utils import timezone
from rest_framework.exceptions import ParseError

from cas.models import Account, AccountRefer
from billing.models import Subscription, SubscriptionDetail

logger = logging.getLogger(__name__)


def add_user_subscription(user_id, user_package, default_storage=5*1024*1024):
    expiry_date = None
    storage = default_storage
    if user_package:
        expiry_date = timezone.now() + timedelta(days=user_package.get_charge_date())
        storage = user_package.storage

    user_subscription_detail = SubscriptionDetail(
        user_id=user_id, expiry_date=expiry_date, storage=storage, package=user_package, status=Subscription.ACTIVE,
    )

    try:
        user_subscription = Subscription.objects.get(user_id=user_id)
        if expiry_date:
            user_subscription.expiry_date = max(expiry_date, user_subscription.expiry_date) \
                                            if user_subscription.expiry_date else expiry_date
        # Not overwrite package info if they have had
        if user_package and not user_subscription.package:
            user_subscription.package_id = user_package.id
            user_subscription.functional_level = user_package.functional_level
        user_subscription.storage += storage
        user_subscription.status = Subscription.ACTIVE
    except:
        user_subscription = Subscription(
            user_id=user_id, expiry_date=expiry_date, storage=storage,
            package_id=user_package.id if user_package else None,
            functional_level = user_package.functional_level if user_package else None,
            status=Subscription.ACTIVE
        )
    return user_subscription, user_subscription_detail


def bonus_account_refer(user_account):
    max_bonus_user = 10
    try:
        user_account = Account.objects.get(id=user_account) if isinstance(user_account, str) else user_account
        invited_by_user_account = Account.objects.get(uu_code=user_account.ref_code)
        try:
            # Check AccountRefer
            AccountRefer.objects.get(user_id=user_account.id, invited_by_user_id=invited_by_user_account.id)
            logger.warning('User %s has already had invited_by_user %s'
                           % (user_account.pk, str(invited_by_user_account.pk)))
            return None
        except AccountRefer.DoesNotExist:
            # Case user reach limit
            num_account_refer = AccountRefer.objects.filter(invited_by_user_id=invited_by_user_account.id).count()
            if num_account_refer >= max_bonus_user:
                return None
            # bonus account
            account_refer = AccountRefer(user_id=user_account.id, uu_code=user_account.ref_code,
                                         invited_by_user_id=invited_by_user_account.id)
            # add subscription to refer account
            invited_by_user_subscription, invited_by_user_subscription_detail = add_user_subscription(
                user_id=invited_by_user_account.pk, user_package=None, default_storage=5*1024*1024)
            return account_refer, invited_by_user_subscription, invited_by_user_subscription_detail
    except Account.DoesNotExist:
        raise ParseError("Reference code not found.")

