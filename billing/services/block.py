#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
    Service for communicating with block backend
    # Sync Container Info after purchasing
"""
from billing.exceptions import InternalError
from django.conf import settings
from jose import jwt
from django.utils import timezone
from datetime import timedelta
import logging
import requests

logger = logging.getLogger(__name__)

def sync_container_info(user_id, max_quota):
    current_time = timezone.now()
    data =  {
            "iss": user_id,
            "iat": current_time,
            "exp": current_time + timedelta(hours=2),
            "context": {
                "container": {
                    "max_quota": max_quota,
                }
            }
        }

    try:
        response = requests.post(
            settings.SYNC_CONTAINER_URL,
            data = { "payload" : jwt.encode(data, settings.JWT_PRIVATE_SIGNATURE , algorithm='HS256') }
        )
    except Exception as ex:
        logger.error(ex)
        raise InternalError("Internal Error . Please try again later")

    return response.content

