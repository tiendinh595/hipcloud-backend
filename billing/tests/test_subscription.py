#!/usr/bin/env python
# -*- coding: utf-8 -*-
from rest_framework.test import APIClient
from django.test import TestCase
from cas.models.access_token import AccessToken
from cas.models.account import Account
from billing.models.purchase_package import PurchasePackage
from billing.models.receipt import AndroidReceipt
from billing.models.subscription import Subscription, UserTransaction
from django.test import Client
from django.utils import timezone
from datetime import timedelta
from rest_framework import status
import logging
import json

logger = logging.getLogger(__name__)

class ServiceTestCases(TestCase):
    fixtures = ['billing.json']

    def setUp(self):
        self.client = APIClient()
        self.android_token_consumable = "mjfhfneapdgjjikfjagadaga.AO-J1OxD6lq1APjSxroHwh8snCxJlDfeyNkwbCh2w1GAI5lUcQmE" \
                                        "dO9S7rnGJ9A1mvF9pyw6xdqCVptNjf3zzHCzOj2pGbdmrNz9oqjYLXCWi3G_c72GEsMzkth6INEh0P" \
                                        "mvroe6moD4n4CB3-6Yrf1kiklfQDUfWQ"
        self.android_token_subscription = "pmbecklocbjcmajdddaemlnl.AO-J1OwyNZ9EUqIRVkzZ-jOfcZlqA6nOehkRSVBybEz7tcaNsWP" \
                                          "fq1-oGLJ1QJzKelu8bzrOQhIRtkUcXiyJm0iW4l28sg4bqbuwv2p9_2iwGRiRaM2V5Sp4Yv7K9J" \
                                          "roYorM0EzJa9IprJtHJ18uDEEzErsZnRgt9Q"
        self.receipt_data = "MIIb7wYJKoZIhvcNAQcCoIIb4DCCG9wCAQExCzAJBgUrDgMCGgUAMIILkAYJKoZIhvcNAQcBoIILgQSCC30xggt5MAoCA" \
                            "QgCAQEEAhYAMAoCARQCAQEEAgwAMAsCAQECAQEEAwIBADALAgELAgEBBAMCAQAwCwIBDgIBAQQDAgFrMAsCAQ8CAQEEAwIBADALAgEQAgEB" \
                            "BAMCAQAwCwIBGQIBAQQDAgEDMAwCAQoCAQEEBBYCNCswDQIBDQIBAQQFAgMBhwUwDQIBEwIBAQQFDAMxLjAwDgIBCQIBAQQGAgRQMjQ3MBQCAQM" \
                            "CAQEEDAwKMTYxMTEyMTEzMDAYAgEEAgECBBCoDmxdlpjVTURb7J1IYSx9MBsCAQACAQEEEwwRUHJvZHVjdGlvblNhb" \
                            "mRib3gwHAIBAgIBAQQUDBJjb20udXBib3guYXBwcy5pb3MwHAIBBQIBAQQUeF1Cm9po5N/S9iJ70FE+PPieqxAwHg" \
                            "IBDAIBAQQWFhQyMDE2LTExLTIyVDAzOjM1OjE4WjAeAgESAgEBBBYWFDIwMTMtMDgtMDFUMDc6MDA6MDBaMEkCAQ" \
                            "YCAQEEQYbI8dbZCPFBZIEiqF0di8cz+ncCOoxm2zMCdCBkilDuitd3dj6ZoB+BXDiYKxj/EhWMv3g/YXqCUleFM1" \
                            "ql5s7GMEsCAQcCAQEEQ4dfI+PqvXfMoIprZRPxd3k2pf1RgHLkErHr3TBcE81JRlq7z/NkmtZp/6yQ+qXip2xgog" \
                            "HfV+aFvWH4ZqEZNELZtdIwggFWAgERAgEBBIIBTDGCAUgwCwICBqwCAQEEAhYAMAsCAgatAgEBBAIMADALAgIGsAI" \
                            "BAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEB" \
                            "BAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQIwDAICBq4CAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgax" \
                            "AgEBBAMCAQAwGwICBqcCAQEEEgwQMTAwMDAwMDI1MjI0NjM4ODAbAgIGqQIBAQQSDBAxMDAwMDAwMjUyMjQ2Mzg" \
                            "4MBwCAgamAgEBBBMMEXN1YnNjcmlwdGlvbl81MEdCMB8CAgaoAgEBBBYWFDIwMTYtMTEtMjFUMDY6NTI6MjNaMB" \
                            "8CAgaqAgEBBBYWFDIwMTYtMTEtMjFUMDY6NTI6MjNaMIIBVgIBEQIBAQSCAUwxggFIMAsCAgasAgEBBAIWADAL" \
                            "AgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMA" \
                            "sCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgECMAwCAgauAgEBBAMCA" \
                            "QAwDAICBq8CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBsCAganAgEBBBIMEDEwMDAwMDAyNTIyNDcyODAwGwICBqk" \
                            "CAQEEEgwQMTAwMDAwMDI1MjI0NzI4MDAcAgIGpgIBAQQTDBFzdWJzY3JpcHRpb25fNTBHQjAfAgIGqAIBAQQWF" \
                            "hQyMDE2LTExLTIxVDA2OjU1OjQxWjAfAgIGqgIBAQQWFhQyMDE2LTExLTIxVDA2OjU1OjQxWjCCAVYCARECAQEE" \
                            "ggFMMYIBSDALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwIC" \
                            "BrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEw" \
                            "DAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwMjUyMjQ5Njg0MBsCA" \
                            "gapAgEBBBIMEDEwMDAwMDAyNTIyNDk2ODQwHAICBqYCAQEEEwwRc3Vic2NyaXB0aW9uXzUwR0IwHwICBqgCAQEEFhYUMjAxNi0xMS0yMVQwNzowMTowNVowHwICBqoCAQEEFhYUMjAxNi0xMS0yMVQwNzowMTowNVowggFWAgERAgEBBIIBTDGCAUgwCwICBqwCAQEEAhYAMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQIwDAICBq4CAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwGwICBqcCAQEEEgwQMTAwMDAwMDI1MjI1MDI3NDAbAgIGqQIBAQQSDBAxMDAwMDAwMjUyMjUwMjc0MBwCAgamAgEBB" \
                            "BMMEXN1YnNjcmlwdGlvbl81MEdCMB8CAgaoAgEBBBYWFDIwMTYtMTEtMjFUMDc6MDQ6NTJaMB8CAgaqAgEBBBYWFDIwMTYtMTEtMjFUMDc6MDQ6NTJaMIIBVgIBEQIBAQSCAUwxggFIMAsCAgasAgEBBAIWADALAgIGrQIBAQQCDAAwCwICBrACAQEEAhYAMAsCAgayAgEBBAIMADALAgIGswIBAQQCDAAwCwICBrQCAQEEAgwAMAsCAga1AgEBBAIMADALAgIGtgIBAQQCDAAwDAICBqUCAQEEAwIBATAMAgIGqwIBAQQDAgECMAwCAgauAgEBBAMCAQAwDAICBq8CAQEEAwIBADAMAgIGsQIBAQQDAgEAMBsCAganAgEBBBIMEDEwMDAwMDAyNTIyNTYzMDAwGwICBqkCAQEEEgwQMTAwMDAwMDI1MjI1NjMwMDAcAgIGpgIBAQQTDBFzdWJzY3JpcHRpb25fNTBHQjAfAgIGqAIBAQQWFhQyMDE2LTExLTIxVDA3OjE4OjIzWjAfAgIGqgIBAQQWFhQyMDE2LTExLTIxVDA3OjE4OjIzWjCCAVYCARECAQEEggFMMYIBSDALAgIGrAIBAQQCFgAwCwICBq0CAQEEAgwAMAsCAgawAgEBBAIWADALAgIGsgIBAQQCDAAwCwICBrMCAQEEAgwAMAsCAga0AgEBBAIMADALAgIGtQIBAQQCDAAwCwICBrYCAQEEAgwAMAwCAgalAgEBBAMCAQEwDAICBqsCAQEEAwIBAjAMAgIGrgIBAQQDAgEAMAwCAgavAgEBBAMCAQAwDAICBrECAQEEAwIBADAbAgIGpwIBAQQSDBAxMDAwMDAwMjUyNjA3Nzk0MBsCAgapAgEBBBIMEDEwMDAwMDAyNTI2MDc3OTQwHAICBqYCAQEEEwwRc3Vic2NyaXB0" \
                            "aW9uXzUwR0IwHwICBqgCAQEEFhYUMjAxNi0xMS0yMlQwMzoyNDo1NVowHwICBqoCAQEEFhYUMjAxNi0xMS0yMlQwMzoyNDo1NVowggFWAgERAgEB" \
                            "BIIBTDGCAUgwCwICBqwCAQEEAhYAMAsCAgatAgEBBAIMADALAgIGsAIBAQQCFgAwCwICBrICAQEEAgwAMAsCAgazAgEBBAIMADALAgIGtAIBAQQCDAAwCwICBrUCAQEEAgwAMAsCAga2AgEBBAIMADAMAgIGpQIBAQQDAgEBMAwCAgarAgEBBAMCAQIwDAICBq4CAQEEAwIBADAMAgIGrwIBAQQDAgEAMAwCAgaxAgEBBAMCAQAwGwICBqcCAQEEEgwQMTAwMDAwMDI1MjYwOTE4MDAbAgIGqQIBAQQSDBAxMDAwMDAwMjUyNjA5MTgwMBwCAgamAgEBBBMMEXN1YnNjcmlwdGlvbl81MEdCMB8CAgaoAgEBBBYWFDIwMTYtMTEtMjJUMDM6MzQ6MDVaMB8CAgaqAgEBBBYWFDIwMTYtMTEtMjJUMDM6MzQ6MDVaoIIOZTCCBXwwggRkoAMCAQICCA7rV4fnngmNMA0GCSqGSIb3DQEBBQUAMIGWMQswCQYDVQQGEwJVUzETMBEGA1UECgwKQXBwbGUgSW5jLjEsMCoGA1UECwwjQXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMxRDBCBgNVBAMMO0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MB4XDTE1MTExMzAyMTUwOVoXDTIzMDIwNzIxNDg0N1owgYkxNzA1BgNVBAMMLk1hYyBBcHAgU3RvcmUgYW5kIGlUdW5lcyBTdG9yZSBSZWNlaXB0IFNpZ25pbmcxLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAKXPgf0looFb1oftI9ozHI7iI8ClxCbLPcaf7EoNVYb/pALXl8o5VG19f7JUGJ3ELFJxjmR7gs6JuknWCOW0iHHPP1tGLsbEHbgDqViiBD4heNXbt9COEo2DTFsqaDeTwvK9HsTSoQxKWFKrEuPt3R+YFZA1LcLMEsqNSIH3WHhUa+iMMTYfSgYMR1TzN5C4spKJfV+khUrhwJzguqS7gpdj9CuTwf0+b8rB9Typj1IawCUKdg7e/pn+/8Jr9VterHNRSQhWicxDkMyOgQLQoJe2XLGhaWmHkBBoJiY5uB0Qc7AKXcVz0N92O9gt2Yge4+wHz+KO0NP6JlWB7+IDSSMCAwEAAaOCAdcwggHTMD8GCCsGAQUFBwEBBDMwMTAvBggrBgEFBQcwAYYjaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwMy13d2RyMDQwHQYDVR0OBBYEFJGknPzEdrefoIr0TfWPNl3tKwSFMAwGA1UdEwEB/wQCMAAwHwYDVR0jBBgwFoAUiCcXCam2GGCL7Ou69kdZxVJUo7cwggEeBgNVHSAEggEVMIIBETCCAQ0GCiqGSIb3Y2QFBgEwgf4wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNgYIKwYBBQUHAgEWKmh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5LzAOBgNVHQ8BAf8EBAMCB4AwEAYKKoZIhvdjZAYLAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAA2mG9MuPeNbKwduQpZs0+iMQzCCX+Bc0Y2+vQ+9GvwlktuMhcOAWd/j4tcuBRSsDdu2uP78NS58y60Xa45/H+R3ubFnlbQTXqYZhnb4WiCV52OMD3P86O3GH66Z+GVIXKDgKDrAEDctuaAEOR9zucgF/fLefxoqKm4rAfygIFzZ630npjP49ZjgvkTbsUxn/G4KT8niBqjSl/OnjmtRolqEdWXRFgRi48Ff9Qipz2jZkgDJwYyz+I0AZLpYYMB8r491ymm5WyrWHWhumEL1TKc3GZvMOxx6GUPzo22/SGAGDDaSK+zeGLUR2i0j0I7" \
                            "8oGmcFxuegHs5R0UwYS/HE6gwggQiMIIDCqADAgECAggB3rzEOW2gEDANBgkqhkiG9w0BAQUFADBiMQswCQYDVQQGEwJVUzETMBEGA1UEChMKQXBwbGUgSW5jLjEmMCQGA1UECxMdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxFjAUBgNVBAMTDUFwcGxlIFJvb3QgQ0EwHhcNMTMwMjA3MjE0ODQ3WhcNMjMwMjA3MjE0ODQ3WjCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMo4VKbLVqrIJDlI6Yzu7F+4fyaRvDRTes58Y4Bhd2RepQcjtjn+UC0VVlhwLX7EbsFKhT4v8N6EGqFXya97GP9q+hUSSRUIGayq2yoy7ZZjaFIVPYyK7L9rGJXgA6wBfZcFZ84OhZU3au0Jtq5nzVFkn8Zc0bxXbmc1gHY2pIeBbjiP2CsVTnsl2Fq/ToPBjdKT1RpxtWCcnTNOVfkSWAyGuBYNweV3RY1QSLorLeSUheHoxJ3GaKWwo/xnfnC6AllLd0KRObn1zeFM78A7SIym5SFd/Wpqu6cWNWDS5q3zRinJ6MOL6XnAamFnFbLw/eVovGJfbs+Z3e8bY/6SZasCAwEAAaOBpjCBozAdBgNVHQ4EFgQUiCcXCam2GGCL7Ou69kdZxVJUo7cwDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAuBgNVHR8EJzAlMCOgIaAfhh1odHRwOi8vY3JsLmFwcGxlLmNvbS9yb290LmNybDAOBgNVHQ8BAf8EBAMCAYYwEAYKKoZIhvdjZAYCAQQCBQAwDQYJKoZIhvcNAQEFBQADggEBAE/P71m+LPWybC+P7hOHMugFNahui33JaQy52Re8dyzUZ+L9mm06WVzfgwG9sq4qYXKxr83DRTCPo4MNzh1HtPGTiqN0m6TDmHKHOz6vRQuSVLkyu5AYU2sKThC22R1QbCGAColOV4xrWzw9pv3e9w0jHQtKJoc/upGSTKQZEhltV/V6WId7aIrkhoxK6+JJFKql3VUAqa67SzCu4aCxvCmA5gl35b40ogHKf9ziCuY7uLvsumKV8wVjQYLNDzsdTJWk26v5yZXpT+RN5yaZgem8+bQp0gF6ZuEujPYhisX4eOGBrr/TkJ2prfOv/TgalmcwHFGlXOxxioK0bA8MFR8wggS7MIIDo6ADAgECAgECMA0GCSqGSIb3DQEBBQUAMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTAeFw0wNjA0MjUyMTQwMzZaFw0zNTAyMDkyMTQwMzZaMGIxCzAJBgNVBAYTAlVTMRMwEQYDVQQKEwpBcHBsZSBJbmMuMSYwJAYDVQQLEx1BcHBsZSBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eTEWMBQGA1UEAxMNQXBwbGUgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAOSRqQkfkdseR1DrBe1eeYQt6zaiV0xV7IsZid75S2z1B6siMALoGD74UAnTf0GomPnRymacJGsR0KO75Bsqwx+VnnoMpEeLW9QWNzPLxA9NzhRp0ckZcvVdDtV/X5vyJQO6VY9NXQ3xZDUjFUsVWR2zlPf2nJ7PULrBWFBnjwi0IPfLrCwgb3C2PwEwjLdDzw+dPfMrSSgayP7OtbkO2V4c1ss9tTqt9A8OAJILsSEWLnTVPA3bYharo3GSR1NVwa8vQbP4++NwzeajTEV+H0xrUJZBicR0YgsQg0GHM4qBsTBY7FoEMoxos48d3mVz/2deZbxJ2HafMxRloXeUyS0CAwEAAaOCAXowggF2MA4GA1UdDwEB/wQEAwIBBjAPBgNVHRMBAf8EBTADAQH/MB0GA1UdDgQWBBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjAfBgNVHSMEGDAWgBQr0GlHlHYJ/vRrjS5ApvdHTX8IXjCCAREGA1UdIASCAQgwggEEMIIBAAYJKoZIhvdjZAUBMIHyMCoGCCsGAQUFBwIBFh5odHRwczovL3d3dy5hcHBsZS5jb20vYXBwbGVjYS8wgcMGCCsGAQUFBwICMIG2GoGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wDQYJKoZIhvcNAQEFBQADggEBAFw2mUwteLftjJvc83eb8nbSdzBPwR+Fg4UbmT1HN/Kpm0COLNSxkBLYvvRzm+7SZA/LeU802KI++Xj/a8gH7H05g4tTINM4xLG/mk8Ka/8r/FmnBQl8F0BWER5007eLIztHo9VvJOLr0bdw3w9F4SfK8W147ee1Fxeo3H4iNcol1dkP1mvUoiQjEfehrI9zgWDGG1sJL5Ky+ERI8GA4nhX1PSZnIIozavcNgs/e66Mv+VNqW2TAYzN39zoHLFbr2g8hDtq6cxlPtdk2f8GHVdmnmbkyQvvY1XGefqFStxu9k0IkEirHDx22TZxeY8hLgBdQqorV2uT80AkHN7B1dSExggHLMIIBxwIBATCBozCBljELMAkGA1UEBhMCVVMxEzARBgNVBAoMCkFwcGxlIEluYy4xLDAqBgNVBAsMI0FwcGxlIFdvcmxkd2lkZSBEZXZlbG9wZXIgUmVsYXRpb25zMUQwQgYDVQQDDDtBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9ucyBDZXJ0aWZpY2F0aW9uIEF1dGhvcml0eQIIDutXh+eeCY0wCQYFKw4DAhoFADANBgkqhkiG9w0BAQEFAASCAQCEKq9ak/WjtSjLvzLsyTcfsmmQHaoE0YATwWMpPF+x1yqw/ffcdc0a4j8Om0D9RnvTW9IXWrCWH6MP0J5aNYFAaQ8ASnJ91aDBPyVujelGbUM3rdty6C/ccovk8mDF77q7lD0bDcSeMh0bI0/kaPXZLQH25VQ9vrMlKyd5771aCBYJyuLFn3QAQSp/nePu7tSmeH57BP1UqflLXDMv7ajk8MK7V4JMpS8PEp10dEyFmrpajY96xkHAkwB69vBVoGxFLfuxPaHCMjyGrI7tURpUzpC7Q2UBYHn37qJz72SeaMvx958h+H1bS9lLKES9+FGcXdO1VNSgcWJc68GApCFH"
        self.access_token = "access_token_test_1"
        self.client.credentials(HTTP_AUTHORIZATION=self.access_token)
        self.ios_package = "package_id_ios_test_1"
        self.android_package_con = "package_id_android_consumption_test_1"
        self.android_package_sub = "package_id_android_subscription_test_1"
        self.user_id = "user_id_test_1"

    def test_account_exists(self):
        try:
            self.account = Account.objects.get(id=self.user_id)
        except Exception as ex:
            self.fail(str(ex))

    def test_access_token_exists(self):
        try:
            access_token = AccessToken.objects.get(user_id=self.user_id)
        except Exception as ex:
            self.fail(str(ex))


    def test_package_exists(self):
        try:
            ios_package = PurchasePackage.objects.get(id=self.ios_package)
            android_package = PurchasePackage.objects.get(id=self.android_package_sub)
            android_package = PurchasePackage.objects.get(id=self.android_package_con)
        except Exception as ex:
            self.fail(str(ex))


    def test_validate_apple_receipt_fail(self):
        # Create transaction
        c = Client()
        # Validate receipt
        transaction_id = "transaction_not_found_id"
        validate_response = c.post('/verify_receipt/ios/',
                                   {'transaction_id': transaction_id,
                                    'apple_receipt': self.receipt_data,
                                    'is_sandbox': '1'},
                         HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(validate_response.status_code, 400)


    def test_validate_apple_receipt(self):
        # Create transaction
        c = Client()
        transaction_response = c.post('/init_transaction/', {'package_id': self.ios_package},
                         HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(transaction_response.status_code, 200)
        transaction_content = json.loads(transaction_response.content)
        self.assertEqual('id' in transaction_content, True)
        self.assertEqual('purchase_package' in transaction_content, True)
        self.assertEqual('readable_id' in transaction_content, True)
        self.assertEqual('type' in transaction_content, True)
        self.assertEqual('user' in transaction_content, True)
        # Validate receipt
        transaction_id = transaction_content['id']
        validate_response = c.post('/verify_receipt/ios/',
                                   {'transaction_id': transaction_id,
                                    'apple_receipt': self.receipt_data,
                                    'is_sandbox': '1'},
                         HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(validate_response.status_code, 200)
        receipt_content = json.loads(validate_response.content)
        self.assertEqual('status' in receipt_content, True)
        # Create subscription
        try:
            transaction = UserTransaction.objects.filter(user_id=self.user_id)
            if not transaction:
                self.fail("Transaction not found")
            user_subscription = Subscription.objects.get(user_id=self.user_id)
        except:
            self.fail("Subscription not found")


    def test_validate_apple_receipt_with_upgrade_package(self):
        c = Client()
        current_time = timezone.now()
        package_to_buy = PurchasePackage.objects.get(id=self.ios_package)
        subscription = Subscription(
            user_id=self.user_id,
            expiry_date=current_time + timedelta(days=package_to_buy.get_charge_date()),
            storage=1000,
            functional_level=Subscription.MINIMUM_FUNCTION,
            status=Subscription.ACTIVE
        )
        subscription.save()
        transaction_response = c.post('/init_transaction/', {'package_id': self.ios_package}, HTTP_AUTHORIZATION=self.access_token)

        self.assertEqual(transaction_response.status_code, 200)
        transaction_content = json.loads(transaction_response.content)
        self.assertEqual('id' in transaction_content, True)
        self.assertEqual('purchase_package' in transaction_content, True)
        self.assertEqual('readable_id' in transaction_content, True)
        self.assertEqual('type' in transaction_content, True)
        self.assertEqual('user' in transaction_content, True)
        # Validate receipt
        transaction_id = transaction_content['id']
        validate_response = c.post('/verify_receipt/ios/',
                                   {'transaction_id': transaction_id,
                                    'apple_receipt': self.receipt_data,
                                    'is_sandbox': '1'},
                                   HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(validate_response.status_code, 200)
        receipt_content = json.loads(validate_response.content)
        self.assertEqual('status' in receipt_content, True)
        # Create subscription
        try:
            transaction = UserTransaction.objects.filter(user_id=self.user_id)
            if not transaction:
                self.fail("Transaction not found")
            user_subscription = Subscription.objects.get(user_id=self.user_id)
        except:
            self.fail("Subscription not found")

    def test_validate_apple_receipt_with_downgrade_package(self):
        c = Client()
        current_time = timezone.now()
        package_to_buy = PurchasePackage.objects.get(id=self.ios_package)
        subscription = Subscription(
            user_id=self.user_id,
            expiry_date=current_time + timedelta(days=package_to_buy.get_charge_date()),
            storage=900000000,
            functional_level=Subscription.MINIMUM_FUNCTION,
            status=Subscription.ACTIVE
        )
        subscription.save()
        transaction_response = c.post('/init_transaction/', {'package_id': self.ios_package},
                                      HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(transaction_response.status_code, 400)


    def test_validate_android_receipt_consumable(self):
        # Create transaction
        c = Client()
        transaction_response = c.post('/init_transaction/', {'package_id': self.android_package_con},
                                      HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(transaction_response.status_code, 200)
        transaction_content = json.loads(transaction_response.content)
        self.assertEqual('id' in transaction_content, True)
        self.assertEqual('purchase_package' in transaction_content, True)
        self.assertEqual('readable_id' in transaction_content, True)
        self.assertEqual('type' in transaction_content, True)
        self.assertEqual('user' in transaction_content, True)
        # Validate receipt

        transaction_id = transaction_content['id']
        validate_response = c.post('/verify_receipt/android/',
                                   {'transaction_id': transaction_id,
                                    'android_token': self.android_token_consumable,
                                    'product_id': 'vn.com.upbox.app.and.managed.product1'},
                                   HTTP_AUTHORIZATION=self.access_token)

        self.assertEqual(validate_response.status_code, 200)
        receipt_content = json.loads(validate_response.content)
        self.assertEqual('status' in receipt_content, True)
        # Create subscription
        try:
            android_receipt = AndroidReceipt.objects.get(user_id=self.user_id, kind=AndroidReceipt.CONSUMPTION_KIND)
            transaction = UserTransaction.objects.filter(user_id=self.user_id)
            if not transaction:
                self.fail("Transaction not found")
            user_subscription = Subscription.objects.get(user_id=self.user_id)
        except:
            self.fail("Subscription not found")


    def test_validate_android_receipt_subscription(self):
        # Create transaction
        c = Client()
        transaction_response = c.post('/init_transaction/', {'package_id': self.android_package_sub},
                                      HTTP_AUTHORIZATION=self.access_token)
        self.assertEqual(transaction_response.status_code, 200)
        transaction_content = json.loads(transaction_response.content)
        self.assertEqual('id' in transaction_content, True)
        self.assertEqual('purchase_package' in transaction_content, True)
        self.assertEqual('readable_id' in transaction_content, True)
        self.assertEqual('type' in transaction_content, True)
        self.assertEqual('user' in transaction_content, True)
        # Validate receipt

        transaction_id = transaction_content['id']
        validate_response = c.post('/verify_receipt/android/',
                                   {'transaction_id': transaction_id,
                                    'android_token': self.android_token_subscription,
                                    'product_id': 'vn.com.upbox.app.and.subscription1'},
                                   HTTP_AUTHORIZATION=self.access_token)
        print validate_response.content
        self.assertEqual(validate_response.status_code, 200)
        receipt_content = json.loads(validate_response.content)
        self.assertEqual('status' in receipt_content, True)
        # Create subscription
        try:
            android_receipt = AndroidReceipt.objects.get(user_id=self.user_id,kind=AndroidReceipt.SUBSCRIPTION_KIND)
            transaction = UserTransaction.objects.filter(user_id=self.user_id)
            if not transaction:
                self.fail("Transaction not found")
            user_subscription = Subscription.objects.get(user_id=self.user_id)
        except:
            self.fail("Subscription not found")








