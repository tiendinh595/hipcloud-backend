#!/usr/bin/env python
# -*- coding: utf-8 -*-

from django.test import TestCase
from billing.services.block import sync_container_info
import logging
from django.test import Client
from api.service.notification import firebase_notification

logger = logging.getLogger(__name__)

class ServiceTestCases(TestCase):
    fixtures = ['billing.json']

    def setUp(self):
        pass

    def test_push_token(self):
        self.assertEqual(firebase_notification("fTG2CIpoAU:APA91bE9uhylSXQqUH6qUQ4n1SHYcbrN7gLQ9rJ-wFv4fyJ_EE62T8KWxJBlS5Gl1FUXfA6ALT3E2QrOQsLyMXWMMFFoBML8BkJoDG0Uzi0uOWwQ8uFuYV2IQpH5t56zGTlnyA6R7ChD", "Poop"),
                         True)


    def test_sync_data(self):
        data_response = sync_container_info("swagger-default", 1000001)
        if 'id' in data_response:
            self.assertEqual(True, True)
        else:
            self.fail()


